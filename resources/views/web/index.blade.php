<!DOCTYPE html>
<html lang="vi">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="UTF-8">
{{--    <!-- Google tag (gtag.js) -->--}}
{{--    <script async src="https://www.googletagmanager.com/gtag/js?id=G-ZB1DZKP02B">--}}
{{--    </script>--}}
{{--    <script>--}}
{{--        window.dataLayer = window.dataLayer || [];--}}
{{--        function gtag(){dataLayer.push(arguments);}--}}
{{--        gtag('js', new Date());--}}
{{--        gtag('config', 'G-ZB1DZKP02B');--}}
{{--    </script>--}}
{{--    <!-- Google tag (gtag.js) -->--}}
{{--    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-11354846218"></script>--}}
{{--    <script>--}}
{{--        window.dataLayer = window.dataLayer || [];--}}
{{--        function gtag(){dataLayer.push(arguments);}--}}
{{--        gtag('js', new Date());--}}

{{--        gtag('config', 'AW-11354846218');--}}
{{--    </script>--}}

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-M4W4M8BK');</script>
    <!-- End Google Tag Manager -->

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-16527609268">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-16527609268');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-SC7DJRD1EG">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-SC7DJRD1EG');
    </script>


    <title>Xe ghép Bảo Bảo | Xe Ghép Hà Nội Vĩnh Phúc | Xe Chạy Hà Nội Tam Đảo | Xe Đưa Đón Hà Nội Vĩnh Phúc</title>
    <title>Xe Ghép Hà Nội - Vĩnh Phúc | Xe Chạy Hà Nội - Tam Đảo | Xe Đưa Đón Hà Nội - Vĩnh Phúc</title>
    <title>Xe Ghép Hà Nội - Vĩnh Phúc | Xe Ghép Hà Nội - Vĩnh Phúc Đưa Đón Tận Nơi </title>
    <title>Xe Ghép Hà Nội - Vĩnh Phúc | Hà Nội - Vĩnh Tường | Hà Nội - Vĩnh Yên | Xe Ghép Hà Nội - Vĩnh Phúc Đưa Đón 2 Chiều </title>
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
    <link rel="canonical" href="{{ canonical_url() }}">
    <meta name="robots" content="index, follow">
    <meta name="keywords"
          content="
          xe ghep ha noi tam dao vinh phuc,
          xeghephanoivinhphuc24h.net,
          xeghephanoivinhphuc24h,
          xeghephanoivinhphuc,
          xeghep,
          xe ghep, ha noi vinh phuc, ha noi tam dao, tam dao,
          thue xe tam dao, thue xe ha noi vinh phuc, thue xe ha noi dai lai,
          thuê xe ghép hà nội tam đảo,
          thuê xe ghép hà nội vĩnh phúc,
          thuê xe hà nội vĩnh phúc,
          thuê xe hà nội đại lãi,
          thuê xe flamingo Đại Lải,
          thuê xe hà nội flamingo Đại Lải,
          thue xe ha noi flamingo Đại Lải,
          xe đi chung hà nội vĩnh phúc,
          xe đi chung vĩnh phúc hà nội,
          flamingo Đại Lải,
          xe ghép hà nội vĩnh phúc,
          xe ghép vĩnh phúc hà nội,
          xe chạy hà nội vĩnh yên
    "
    >
    <meta name="description" content="
    Đặt Xe Ghép Vĩnh Phúc - Hà Nội - Tam Đảo, Xe Đi Tour, Xe Đi Công Tác Giá Rẻ - Các Dòng Xe Mới Nhất. Các Dòng Xe Đa Dạng.
    Đón - Trả Khách Tận Nơi.">
    <meta name="description" content="
    Xe Ghép Hà Nội - Vĩnh Phúc Đưa Đón 2 Chiều, Đặt xe, bao xe trọn gói, thuê xe trực tuyến, thuê xe ghép, ship hàng,
    Xe Ghép Bảo Bảo - Uy Tín - Giá Cả Cạnh Tranh - Xe Đời Mới Nhất - Phục Vụ Xuyên Suốt 24h.">
    <script type='text/javascript'>window.ladi_viewport = function () {
            var screen_width = window.ladi_screen_width || window.screen.width;
            var width = window.outerWidth > 0 ? window.outerWidth : screen_width;
            var widthDevice = width;
            var is_desktop = width >= 768;
            var content = "";
            if (typeof window.ladi_is_desktop == "undefined" || window.ladi_is_desktop == undefined) {
                window.ladi_is_desktop = is_desktop;
            }
            if (!is_desktop) {
                widthDevice = 420;
            } else {
                widthDevice = 960;
            }
            content = "width=" + widthDevice + ", user-scalable=no";
            var scale = 1;
            if (!is_desktop && widthDevice != screen_width && screen_width > 0) {
                scale = screen_width / widthDevice;
            }
            if (scale != 1) {
                content += ", initial-scale=" + scale + ", minimum-scale=" + scale + ", maximum-scale=" + scale;
            }
            var docViewport = document.getElementById("viewport");
            if (!docViewport) {
                docViewport = document.createElement("meta");
                docViewport.setAttribute("id", "viewport");
                docViewport.setAttribute("name", "viewport");
                document.head.appendChild(docViewport);
            }
            docViewport.setAttribute("content", content);
        };
        window.ladi_viewport();
        window.ladi_fbq_data = [];
        window.ladi_fbq = function (track_name, conversion_name, data, event_data) {
            window.ladi_fbq_data.push([track_name, conversion_name, data, event_data]);
        };</script>
    <meta property="og:url" content="{{ env('APP_URL') }}"/>
    <meta property="og:title" content="Xe ghép Bảo Bảo"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image"
          content="/web/car1.jpeg">
    <meta property="og:description"
          content="Xe Ghép Hà Nội - Vĩnh Phúc, Đặt xe - Bao xe trọn gói, Thuê xe trực tuyến - Thuê xe ghép, Ship hàng"/>
    <meta property="og:description" content="Đặt Xe Ghép Vĩnh Phúc - Hà Nội - Tam Đảo, Xe Đi Tour, Xe Đi Công Tác Giá Rẻ - Các Dòng Xe Mới Nhất. Các Dòng Xe Đa Dạng.
    Đón - Trả Khách Tận Nơi."/>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="shortcut icon" type="image/png"
          href="/web/images/favicon/toyota-vios-20211111012556.png"/>
    <link rel="icon" type="image/png" href="/web/images/favicon/toyota-vios-20211111012556.png"/>
    <link rel="dns-prefetch">
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <link rel="preconnect" href="https://api.form.ladipage.com/" crossorigin>
    <link rel="preconnect" href="https://a.ladipage.com/" crossorigin>
    <link rel="preconnect" href="https://api.ladisales.com/" crossorigin>
    <link rel="preload"
          href="https://fonts.googleapis.com/css?family=Crimson%20Pro:bold,regular|Tinos:bold,regular&amp;display=swap"
          as="style" onload="this.onload = null; this.rel = 'stylesheet';">
    <link rel="preload" href="/web/images/v2/source/ladipage.vi.minb1b7.js" as="script">
    <link rel="stylesheet" href="/web/css/carousel.css">
    <script defer src="/web/js/carousel.js"></script>
    <style id="style_ladi"
           type="text/css">a, abbr, acronym, address, applet, article, aside, audio, b, big, blockquote, body, button, canvas, caption, center, cite, code, dd, del, details, dfn, div, dl, dt, em, embed, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, input, ins, kbd, label, legend, li, mark, menu, nav, object, ol, output, p, pre, q, ruby, s, samp, section, select, small, span, strike, strong, sub, summary, sup, table, tbody, td, textarea, tfoot, th, thead, time, tr, tt, u, ul, var, video {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
            box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
            display: block
        }

        body {
            line-height: 1
        }

        a {
            text-decoration: none
        }

        ol, ul {
            list-style: none
        }

        blockquote, q {
            quotes: none
        }

        blockquote:after, blockquote:before, q:after, q:before {
            content: '';
            content: none
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        .ladi-html strong {
            font-weight: 700
        }

        .ladi-html em {
            font-style: italic
        }

        .ladi-html a {
            text-decoration: underline
        }

        body {
            font-size: 12px;
            -ms-text-size-adjust: none;
            -moz-text-size-adjust: none;
            -o-text-size-adjust: none;
            -webkit-text-size-adjust: none;
            background-color: #fff
        }

        .ladi-loading {
            width: 80px;
            height: 80px;
            z-index: 900000000000;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            overflow: hidden
        }

        .ladi-loading div {
            position: absolute;
            width: 6px;
            height: 6px;
            background: #fff;
            border-radius: 50%;
            animation: ladi-loading 1.2s linear infinite
        }

        .ladi-loading div:nth-child(1) {
            animation-delay: 0s;
            top: 37px;
            left: 66px
        }

        .ladi-loading div:nth-child(2) {
            animation-delay: -.1s;
            top: 22px;
            left: 62px
        }

        .ladi-loading div:nth-child(3) {
            animation-delay: -.2s;
            top: 11px;
            left: 52px
        }

        .ladi-loading div:nth-child(4) {
            animation-delay: -.3s;
            top: 7px;
            left: 37px
        }

        .ladi-loading div:nth-child(5) {
            animation-delay: -.4s;
            top: 11px;
            left: 22px
        }

        .ladi-loading div:nth-child(6) {
            animation-delay: -.5s;
            top: 22px;
            left: 11px
        }

        .ladi-loading div:nth-child(7) {
            animation-delay: -.6s;
            top: 37px;
            left: 7px
        }

        .ladi-loading div:nth-child(8) {
            animation-delay: -.7s;
            top: 52px;
            left: 11px
        }

        .ladi-loading div:nth-child(9) {
            animation-delay: -.8s;
            top: 62px;
            left: 22px
        }

        .ladi-loading div:nth-child(10) {
            animation-delay: -.9s;
            top: 66px;
            left: 37px
        }

        .ladi-loading div:nth-child(11) {
            animation-delay: -1s;
            top: 62px;
            left: 52px
        }

        .ladi-loading div:nth-child(12) {
            animation-delay: -1.1s;
            top: 52px;
            left: 62px
        }

        @keyframes ladi-loading {
            0%, 100%, 20%, 80% {
                transform: scale(1)
            }
            50% {
                transform: scale(1.5)
            }
        }

        .overflow-hidden {
            overflow: hidden
        }

        .ladi-transition {
            transition: all 150ms linear 0s
        }

        .opacity-0 {
            opacity: 0
        }

        .height-0 {
            height: 0 !important
        }

        .transition-readmore {
            transition: height 350ms linear 0s
        }

        .transition-collapse {
            transition: height 150ms linear 0s
        }

        .transition-parent-collapse-height {
            transition: height 150ms linear 0s
        }

        .transition-parent-collapse-top {
            transition: top 150ms linear 0s
        }

        .pointer-events-none {
            pointer-events: none
        }

        .ladi-google-recaptcha-checkbox {
            position: absolute;
            display: inline-block;
            transform: translateY(-100%);
            margin-top: -5px;
            z-index: 90000010
        }

        .ladipage-message {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 10000000000;
            background: rgba(0, 0, 0, .3)
        }

        .ladipage-message .ladipage-message-box {
            width: 400px;
            max-width: calc(100% - 50px);
            height: 160px;
            border: 1px solid rgba(0, 0, 0, .3);
            background-color: #fff;
            position: fixed;
            top: calc(50% - 155px);
            left: 0;
            right: 0;
            margin: auto;
            border-radius: 10px
        }

        .ladipage-message .ladipage-message-box span {
            display: block;
            background-color: rgba(6, 21, 40, .05);
            color: #000;
            padding: 12px 15px;
            font-weight: 600;
            font-size: 16px;
            line-height: 16px;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px
        }

        .ladipage-message .ladipage-message-box .ladipage-message-text {
            display: -webkit-box;
            font-size: 14px;
            padding: 0 20px;
            margin-top: 10px;
            line-height: 18px;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .ladipage-message .ladipage-message-box .ladipage-message-close {
            display: block;
            position: absolute;
            right: 15px;
            bottom: 10px;
            margin: 0 auto;
            padding: 10px 0;
            border: none;
            width: 80px;
            text-transform: uppercase;
            text-align: center;
            color: #000;
            background-color: #e6e6e6;
            border-radius: 5px;
            text-decoration: none;
            font-size: 14px;
            line-height: 14px;
            font-weight: 600;
            cursor: pointer
        }

        .ladi-wraper {
            width: 100%;
            height: 100%;
            min-height: 100%;
            overflow: hidden
        }

        .ladi-section {
            margin: 0 auto;
            position: relative
        }

        .ladi-section[data-tab-id] {
            display: none
        }

        .ladi-section.selected[data-tab-id] {
            display: block
        }

        .ladi-section .ladi-section-arrow-down {
            position: absolute;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-section .ladi-section-close {
            position: absolute;
            top: 0;
            right: 0;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-section .ladi-section-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none;
            overflow: hidden
        }

        .ladi-container {
            position: relative;
            margin: 0 auto;
            height: 100%
        }

        .ladi-element {
            position: absolute
        }

        .ladi-overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-survey {
            width: 100%;
            display: inline-block
        }

        .ladi-survey .ladi-survey-option {
            cursor: pointer;
            position: relative;
            display: inline-block;
            text-decoration-line: inherit;
            -webkit-text-decoration-line: inherit;
            transition: inherit;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-survey .ladi-survey-option-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none;
            transition: inherit
        }

        .ladi-survey .ladi-survey-option-label {
            display: block;
            text-decoration-line: inherit;
            -webkit-text-decoration-line: inherit;
            position: relative
        }

        .ladi-survey .ladi-survey-option-image {
            background-size: cover;
            background-position: center center;
            pointer-events: none;
            vertical-align: middle;
            border-radius: inherit;
            position: relative
        }

        .ladi-survey .ladi-survey-button-next {
            display: block
        }

        .ladi-survey .ladi-survey-button-next.empty {
            display: none
        }

        .ladi-survey .ladi-survey-button-next.no-select {
            display: none
        }

        .ladi-survey .ladi-survey-button-next button {
            background-color: #fff;
            border: 1px solid #eee;
            padding: 5px 10px;
            cursor: pointer;
            border-radius: 2px
        }

        .ladi-survey .ladi-survey-button-next button:active {
            transform: translateY(2px);
            transition: transform .2s linear
        }

        .ladi-carousel {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
            touch-action: pan-y
        }

        .ladi-carousel .ladi-carousel-content {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            transition: left 350ms ease-in-out
        }

        .ladi-carousel .ladi-carousel-arrow {
            position: absolute;
            top: calc(50% - (33px) / 2);
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-carousel .ladi-carousel-arrow-left {
            left: 5px;
            transform: rotate(180deg);
            -webkit-transform: rotate(180deg)
        }

        .ladi-carousel .ladi-carousel-arrow-right {
            right: 5px
        }

        .ladi-gallery {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-gallery .ladi-gallery-view {
            position: absolute;
            overflow: hidden;
            touch-action: pan-y
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            width: 100%;
            height: 100%;
            position: relative;
            display: none;
            transition: transform .5s ease-in-out;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-perspective: 1000px;
            perspective: 1000px
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.play-video {
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.play-video:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 60px;
            height: 60px;
            background: url(https://w.ladicdn.com/source/ladipage-play.svg) no-repeat center center;
            background-size: contain;
            pointer-events: none;
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.next, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected.right {
            left: 0;
            transform: translate3d(100%, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.prev, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected.left {
            left: 0;
            transform: translate3d(-100%, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.next.left, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.prev.right, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected {
            left: 0;
            transform: translate3d(0, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .next, .ladi-gallery .ladi-gallery-view > .prev, .ladi-gallery .ladi-gallery-view > .selected {
            display: block
        }

        .ladi-gallery .ladi-gallery-view > .selected {
            left: 0
        }

        .ladi-gallery .ladi-gallery-view > .next, .ladi-gallery .ladi-gallery-view > .prev {
            position: absolute;
            top: 0;
            width: 100%
        }

        .ladi-gallery .ladi-gallery-view > .next {
            left: 100%
        }

        .ladi-gallery .ladi-gallery-view > .prev {
            left: -100%
        }

        .ladi-gallery .ladi-gallery-view > .next.left, .ladi-gallery .ladi-gallery-view > .prev.right {
            left: 0
        }

        .ladi-gallery .ladi-gallery-view > .selected.left {
            left: -100%
        }

        .ladi-gallery .ladi-gallery-view > .selected.right {
            left: 100%
        }

        .ladi-gallery .ladi-gallery-control {
            position: absolute;
            overflow: hidden;
            touch-action: pan-y
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-view {
            width: 100%
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control {
            top: 0;
            width: 100%
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-view {
            top: 0;
            width: 100%
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control {
            width: 100%;
            bottom: 0
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-view {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-view {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control {
            height: 100%;
            right: 0
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow {
            position: absolute;
            top: calc(50% - (33px) / 2);
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow-left {
            left: 5px;
            transform: rotate(180deg);
            -webkit-transform: rotate(180deg)
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow-right {
            right: 5px
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-arrow {
            position: absolute;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow, .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow {
            top: calc(50% - (33px) / 2)
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow-left {
            left: 0;
            transform: rotate(180deg) scale(.6);
            -webkit-transform: rotate(180deg) scale(.6)
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow-right {
            right: 0;
            transform: scale(.6);
            -webkit-transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow-left {
            left: 0;
            transform: rotate(180deg) scale(.6);
            -webkit-transform: rotate(180deg) scale(.6)
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow-right {
            right: 0;
            transform: scale(.6);
            -webkit-transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow, .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow {
            left: calc(50% - (33px) / 2)
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow-left {
            top: 0;
            transform: scale(.6) rotate(270deg);
            -webkit-transform: scale(.6) rotate(270deg)
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow-right {
            bottom: 0;
            transform: scale(.6) rotate(90deg);
            -webkit-transform: scale(.6) rotate(90deg)
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow-left {
            top: 0;
            transform: scale(.6) rotate(270deg);
            -webkit-transform: scale(.6) rotate(270deg)
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow-right {
            bottom: 0;
            transform: scale(.6) rotate(90deg);
            -webkit-transform: scale(.6) rotate(90deg)
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box {
            position: relative
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-box {
            display: -webkit-inline-flex;
            display: inline-flex;
            left: 0;
            transition: left 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-box {
            display: -webkit-inline-flex;
            display: inline-flex;
            left: 0;
            transition: left 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-grid;
            top: 0;
            transition: top 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-grid;
            top: 0;
            transition: top 150ms ease-in-out
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            float: left;
            position: relative;
            cursor: pointer;
            filter: invert(15%)
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item.play-video:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 30px;
            height: 30px;
            background: url(https://w.ladicdn.com/source/ladipage-play.svg) no-repeat center center;
            background-size: contain;
            pointer-events: none;
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item:hover {
            filter: none
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item.selected {
            filter: none
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item:last-child {
            margin-right: 0 !important;
            margin-bottom: 0 !important
        }

        .ladi-table {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: auto
        }

        .ladi-table table {
            width: 100%
        }

        .ladi-table table td {
            vertical-align: middle
        }

        .ladi-table tbody td {
            word-break: break-word
        }

        .ladi-table table td img {
            cursor: pointer;
            width: 100%
        }

        .ladi-box {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-tabs {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-tabs .ladi-tabs-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-tabs > .ladi-element[data-index] {
            display: none
        }

        .ladi-tabs > .ladi-element.selected[data-index] {
            display: block
        }

        .ladi-frame {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-frame .ladi-frame-background {
            height: 100%;
            width: 100%;
            pointer-events: none;
            transition: inherit
        }

        .ladi-banner {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-banner .ladi-banner-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        #SECTION_POPUP .ladi-container {
            z-index: 90000070
        }

        #SECTION_POPUP .ladi-container > .ladi-element {
            z-index: 90000070;
            position: fixed;
            display: none
        }

        #SECTION_POPUP .ladi-container > .ladi-element[data-fixed-close=true] {
            position: relative !important
        }

        #SECTION_POPUP .ladi-container > .ladi-element.hide-visibility {
            display: block !important;
            visibility: hidden !important
        }

        #SECTION_POPUP .popup-close {
            position: absolute;
            right: 0;
            top: 0;
            z-index: 9000000080;
            cursor: pointer
        }

        .ladi-popup {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-popup .ladi-popup-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-countdown {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-countdown .ladi-countdown-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            display: table;
            pointer-events: none
        }

        .ladi-countdown .ladi-countdown-text {
            position: absolute;
            width: 100%;
            height: 100%;
            text-decoration: inherit;
            display: table;
            pointer-events: none
        }

        .ladi-countdown .ladi-countdown-text span {
            display: table-cell;
            vertical-align: middle
        }

        .ladi-countdown > .ladi-element {
            text-decoration: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            position: relative;
            display: inline-block
        }

        .ladi-countdown > .ladi-element:last-child {
            margin-right: 0 !important
        }

        .ladi-button {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-button:active {
            transform: translateY(2px);
            transition: transform .2s linear
        }

        .ladi-button .ladi-button-background {
            height: 100%;
            width: 100%;
            pointer-events: none;
            transition: inherit
        }

        .ladi-button > .ladi-button-headline-01, .ladi-button > .ladi-button-shape {
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            display: table;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-button > .ladi-button-shape .ladi-shape {
            margin: auto;
            top: 0;
            bottom: 0
        }

        .ladi-button > .ladi-button-headline-01 .ladi-headline {
            display: table-cell;
            vertical-align: middle
        }

        .ladi-collection {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-collection.carousel {
            overflow: hidden
        }

        .ladi-collection .ladi-collection-content {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            transition: left 350ms ease-in-out
        }

        .ladi-collection .ladi-collection-content .ladi-collection-item {
            display: block;
            position: relative;
            float: left
        }

        .ladi-collection .ladi-collection-content .ladi-collection-page {
            float: left
        }

        .ladi-collection .ladi-collection-arrow {
            position: absolute;
            top: calc(50% - (33px) / 2);
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-collection .ladi-collection-arrow-left {
            left: 5px;
            transform: rotate(180deg);
            -webkit-transform: rotate(180deg)
        }

        .ladi-collection .ladi-collection-arrow-right {
            right: 5px
        }

        .ladi-collection .ladi-collection-button-next {
            position: absolute;
            bottom: -40px;
            right: 0;
            left: 0;
            margin: auto;
            cursor: pointer;
            z-index: 90000040;
            transform: rotate(90deg);
            -webkit-transform: rotate(90deg)
        }

        .ladi-form {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-form > .ladi-element {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form .ladi-button > .ladi-button-headline-01 {
            color: initial;
            font-size: initial;
            font-weight: initial;
            text-transform: initial;
            text-decoration: initial;
            font-style: initial;
            text-align: initial;
            letter-spacing: initial;
            line-height: initial
        }

        .ladi-form > .ladi-element .ladi-form-item-container {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > [data-quantity=true] .ladi-form-item-container {
            overflow: hidden
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item-background {
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background-size: 9px 6px !important;
            background-position: right .5rem center;
            background-repeat: no-repeat
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-2 {
            width: calc(100% / 2 - 5px);
            max-width: calc(100% / 2 - 5px);
            min-width: calc(100% / 2 - 5px)
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-2:nth-child(3) {
            margin-left: 7.5px
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3 {
            width: calc(100% / 3 - 5px);
            max-width: calc(100% / 3 - 5px);
            min-width: calc(100% / 3 - 5px)
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(3) {
            margin-left: 7.5px
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(4) {
            margin-left: 7.5px
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select option {
            color: initial
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control:not(.ladi-form-control-select) {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
            text-transform: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select:not([data-selected=""]) {
            text-decoration: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            vertical-align: middle
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span {
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked=true] {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked=false] {
            text-transform: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form .ladi-form-item-container {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-form .ladi-form-item-title-value {
            font-weight: 700;
            word-break: break-word
        }

        .ladi-form .ladi-form-label-container {
            position: relative;
            width: 100%
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item {
            display: inline-block;
            cursor: pointer;
            position: relative;
            border-radius: 0 !important;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.image {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.no-value {
            display: none !important
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.text.disabled {
            opacity: .35
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.image.disabled {
            opacity: .2
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.color.disabled {
            opacity: .15
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.selected:before {
            content: '';
            width: 0;
            height: 0;
            bottom: -1px;
            right: -1px;
            position: absolute;
            border-width: 0 0 15px 15px;
            border-color: transparent;
            border-style: solid
        }

        .ladi-form .ladi-form-label-container .ladi-form-label-item.selected:after {
            content: '';
            background-image: url("data:image/svg+xml;utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' enable-background='new 0 0 12 12' viewBox='0 0 12 12' x='0' fill='%23fff' y='0'%3E%3Cg%3E%3Cpath d='m5.2 10.9c-.2 0-.5-.1-.7-.2l-4.2-3.7c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l3.4 3 5.1-7c .3-.4 1-.5 1.4-.2s.5 1 .2 1.4l-5.7 7.9c-.2.2-.4.4-.7.4 0-.1 0-.1-.1-.1z'%3E%3C/path%3E%3C/g%3E%3C/svg%3E");
            background-repeat: no-repeat;
            background-position: bottom right;
            width: 7px;
            height: 7px;
            bottom: 0;
            right: 0;
            position: absolute
        }

        .ladi-form .ladi-form-item {
            width: 100%;
            height: 100%;
            position: absolute
        }

        .ladi-form .ladi-form-item-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox {
            height: auto
        }

        .ladi-form .ladi-form-item .ladi-form-control {
            background-color: transparent;
            min-width: 100%;
            min-height: 100%;
            max-width: 100%;
            max-height: 100%;
            width: 100%;
            height: 100%;
            padding: 0 5px;
            color: inherit;
            font-size: inherit;
            border: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox {
            padding: 10px 5px
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-vertical .ladi-form-checkbox-item {
            margin-top: 0 !important;
            margin-left: 0 !important;
            margin-right: 0 !important;
            display: table;
            border: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-horizontal .ladi-form-checkbox-item {
            margin-top: 0 !important;
            margin-left: 0 !important;
            margin-right: 10px !important;
            display: inline-block;
            border: none;
            position: relative
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item input {
            vertical-align: middle;
            width: 13px;
            height: 13px;
            display: table-cell;
            margin-right: 5px
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span {
            display: table-cell;
            cursor: default;
            vertical-align: middle;
            word-break: break-word
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-horizontal .ladi-form-checkbox-item input {
            position: absolute;
            top: 4px
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-horizontal .ladi-form-checkbox-item span {
            padding-left: 18px
        }

        .ladi-form .ladi-form-item textarea.ladi-form-control {
            resize: none;
            padding: 5px
        }

        .ladi-form .ladi-button {
            cursor: pointer
        }

        .ladi-form .ladi-button .ladi-headline {
            cursor: pointer;
            user-select: none
        }

        .ladi-combobox {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-combobox .ladi-combobox-item-container {
            position: absolute;
            width: 100%;
            height: 100%;
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-combobox .ladi-combobox-item-container .ladi-combobox-item-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-combobox .ladi-combobox-item-container .ladi-combobox-item {
            width: 100%;
            height: 100%;
            position: absolute;
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit
        }

        .ladi-combobox .ladi-combobox-item-container .ladi-combobox-item .ladi-combobox-control option {
            color: initial
        }

        .ladi-combobox .ladi-combobox-item-container .ladi-combobox-item .ladi-combobox-control {
            background-color: transparent;
            min-width: 100%;
            min-height: 100%;
            max-width: 100%;
            max-height: 100%;
            width: 100%;
            height: 100%;
            padding: 0 5px;
            font-size: inherit;
            border: none;
            text-transform: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background-size: 9px 6px !important;
            background-position: right .5rem center;
            background-repeat: no-repeat
        }

        .ladi-combobox .ladi-combobox-item-container .ladi-combobox-item .ladi-combobox-control:not([data-selected=""]) {
            text-decoration: inherit
        }

        .ladi-cart {
            position: absolute;
            width: 100%;
            font-size: 12px
        }

        .ladi-cart .ladi-cart-row {
            position: relative;
            display: inline-table;
            width: 100%
        }

        .ladi-cart .ladi-cart-row:after {
            content: '';
            position: absolute;
            left: 0;
            bottom: 0;
            height: 1px;
            width: 100%;
            background: #dcdcdc
        }

        .ladi-cart .ladi-cart-no-product {
            text-align: center;
            font-size: 16px;
            vertical-align: middle
        }

        .ladi-cart .ladi-cart-image {
            width: 16%;
            vertical-align: middle;
            position: relative;
            text-align: center
        }

        .ladi-cart .ladi-cart-image img {
            max-width: 100%
        }

        .ladi-cart .ladi-cart-title {
            vertical-align: middle;
            padding: 0 5px;
            word-break: break-all
        }

        .ladi-cart .ladi-cart-title .ladi-cart-title-name {
            display: block;
            margin-bottom: 5px;
            word-break: break-word
        }

        .ladi-cart .ladi-cart-title .ladi-cart-title-variant {
            font-weight: 700;
            display: block;
            word-break: break-word
        }

        .ladi-cart .ladi-cart-image .ladi-cart-image-quantity {
            position: absolute;
            top: -3px;
            right: -5px;
            background: rgba(150, 149, 149, .9);
            width: 20px;
            height: 20px;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            line-height: 20px
        }

        .ladi-cart .ladi-cart-quantity {
            width: 70px;
            vertical-align: middle;
            text-align: center
        }

        .ladi-cart .ladi-cart-quantity-content {
            display: -webkit-inline-flex;
            display: inline-flex
        }

        .ladi-cart .ladi-cart-quantity input {
            width: 24px;
            text-align: center;
            height: 22px;
            -moz-appearance: textfield;
            border-top: 1px solid #dcdcdc;
            border-bottom: 1px solid #dcdcdc
        }

        .ladi-cart .ladi-cart-quantity input::-webkit-inner-spin-button, .ladi-cart .ladi-cart-quantity input::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0
        }

        .ladi-cart .ladi-cart-quantity button {
            border: 1px solid #dcdcdc;
            cursor: pointer;
            text-align: center;
            width: 21px;
            height: 22px;
            position: relative;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-cart .ladi-cart-quantity button:active {
            transform: translateY(2px);
            transition: transform .2s linear
        }

        .ladi-cart .ladi-cart-quantity button span {
            font-size: 18px;
            position: relative;
            left: .5px
        }

        .ladi-cart .ladi-cart-quantity button:first-child span {
            top: -1.2px
        }

        .ladi-cart .ladi-cart-price {
            width: 100px;
            vertical-align: middle;
            text-align: right;
            padding: 0 10px 0 5px
        }

        .ladi-cart .ladi-cart-row.has-promotion .ladi-cart-price span {
            text-decoration: line-through;
            display: block;
            margin-bottom: 3px
        }

        .ladi-cart .ladi-cart-row.has-promotion .ladi-cart-price span.price-compare {
            text-decoration: none;
            color: #e85d04;
            font-weight: 700;
            margin-bottom: 0
        }

        .ladi-cart .ladi-cart-row.has-promotion .ladi-cart-title span.promotion-name {
            margin-top: 5px;
            display: block;
            word-break: break-word
        }

        .ladi-cart .ladi-cart-action {
            width: 28px;
            vertical-align: middle;
            text-align: center
        }

        .ladi-cart .ladi-cart-action button {
            border: 1px solid #dcdcdc;
            cursor: pointer;
            text-align: center;
            width: 25px;
            height: 22px;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-cart .ladi-cart-action button:active {
            transform: translateY(2px);
            transition: transform .2s linear
        }

        .ladi-cart .ladi-cart-action button span {
            font-size: 13px;
            position: relative;
            top: .5px
        }

        .ladi-video {
            position: absolute;
            width: 100%;
            height: 100%;
            cursor: pointer;
            overflow: hidden
        }

        .ladi-video .ladi-video-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none
        }

        .ladi-group {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        .ladi-group-2 {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        .ladi-button-group {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-button-group > .ladi-element {
            transition: inherit
        }

        .ladi-button-group > .ladi-element > .ladi-button {
            transition: inherit
        }

        .ladi-shape {
            position: absolute;
            width: 100%;
            height: 100%;
            pointer-events: none
        }

        .ladi-html-code {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-image {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
            pointer-events: none
        }

        .ladi-image .ladi-image-background {
            background-repeat: no-repeat;
            background-position: left top;
            background-size: cover;
            background-attachment: scroll;
            background-origin: content-box;
            position: absolute;
            margin: 0 auto;
            width: 100%;
            height: 100%;
            pointer-events: none
        }

        .ladi-headline {
            width: 100%;
            display: inline-block;
            background-size: cover;
            background-position: center center;
            line-height: 1.4;
        }

        .ladi-headline a {
            text-decoration: underline
        }

        .ladi-paragraph {
            width: 100%;
            display: inline-block
        }

        .ladi-paragraph a {
            text-decoration: underline
        }

        .ladi-list-paragraph {
            width: 100%;
            display: inline-block
        }

        .ladi-list-paragraph a {
            text-decoration: underline
        }

        .ladi-list-paragraph ul li {
            position: relative;
            counter-increment: linum
        }

        .ladi-list-paragraph ul li:before {
            position: absolute;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            left: 0
        }

        .ladi-list-paragraph ul li:last-child {
            padding-bottom: 0 !important
        }

        .ladi-line {
            position: relative
        }

        .ladi-line .ladi-line-container {
            border-bottom: 0 !important;
            border-right: 0 !important;
            width: 100%;
            height: 100%
        }

        a[data-action] {
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            cursor: pointer
        }

        a:visited {
            color: inherit
        }

        a:link {
            color: inherit
        }

        .button-unmute {
            cursor: pointer;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto
        }

        .button-unmute div {
            background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2036%2036%22%20width%3D%22100%25%22%20height%3D%22100%25%22%20fill%3D%22%23fff%22%3E%3Cpath%20d%3D%22m%2021.48%2C17.98%20c%200%2C-1.77%20-1.02%2C-3.29%20-2.5%2C-4.03%20v%202.21%20l%202.45%2C2.45%20c%20.03%2C-0.2%20.05%2C-0.41%20.05%2C-0.63%20z%20m%202.5%2C0%20c%200%2C.94%20-0.2%2C1.82%20-0.54%2C2.64%20l%201.51%2C1.51%20c%20.66%2C-1.24%201.03%2C-2.65%201.03%2C-4.15%200%2C-4.28%20-2.99%2C-7.86%20-7%2C-8.76%20v%202.05%20c%202.89%2C.86%205%2C3.54%205%2C6.71%20z%20M%209.25%2C8.98%20l%20-1.27%2C1.26%204.72%2C4.73%20H%207.98%20v%206%20H%2011.98%20l%205%2C5%20v%20-6.73%20l%204.25%2C4.25%20c%20-0.67%2C.52%20-1.42%2C.93%20-2.25%2C1.18%20v%202.06%20c%201.38%2C-0.31%202.63%2C-0.95%203.69%2C-1.81%20l%202.04%2C2.05%201.27%2C-1.27%20-9%2C-9%20-7.72%2C-7.72%20z%20m%207.72%2C.99%20-2.09%2C2.08%202.09%2C2.09%20V%209.98%20z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            width: 60px;
            height: 60px;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            background-color: rgba(0, 0, 0, .5);
            border-radius: 100%;
            background-size: 90%;
            background-repeat: no-repeat;
            background-position: center center
        }

        [data-opacity="0"] {
            opacity: 0
        }

        [data-hidden=true] {
            display: none
        }

        [data-action=true] {
            cursor: pointer
        }

        .ladi-hidden {
            display: none
        }

        .backdrop-popup {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 90000060
        }

        .backdrop-dropbox {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 90000080
        }

        .lightbox-screen {
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            z-index: 9000000080;
            background: rgba(0, 0, 0, .5)
        }

        .lightbox-screen .lightbox-close {
            position: absolute;
            z-index: 9000000090;
            cursor: pointer
        }

        .lightbox-screen .lightbox-hidden {
            display: none
        }

        .ladi-animation-hidden {
            visibility: hidden !important
        }

        .ladi-lazyload {
            background-image: none !important
        }

        .ladi-list-paragraph ul li.ladi-lazyload:before {
            background-image: none !important
        }

        .ladi-cart-number {
            position: absolute;
            top: -2px;
            right: -7px;
            background: #f36e36;
            text-align: center;
            width: 18px;
            height: 18px;
            line-height: 18px;
            font-size: 11px;
            font-weight: 700;
            color: #fff;
            border-radius: 100%
        }

        .ladi-form-quantity {
            display: -webkit-inline-flex;
            display: inline-flex;
            border-color: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item.ladi-form-quantity .ladi-form-control {
            text-align: center;
            pointer-events: none;
            -moz-appearance: textfield;
            width: calc(100% - 45px);
            padding: 0;
            min-width: 24px;
            border-top: 1px hidden;
            border-bottom: 1px hidden;
            border-color: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item.ladi-form-quantity input::-webkit-inner-spin-button, .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item.ladi-form-quantity input::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0
        }

        .ladi-form-quantity button {
            border: 1px hidden;
            border-color: inherit;
            cursor: pointer;
            text-align: center;
            width: 30px;
            height: 100%;
            position: relative;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-form-quantity button span {
            font-size: 18px;
            position: relative;
            left: .5px
        }

        .ladi-form-quantity button:first-child span {
            top: -1.2px
        }

        .ladi-form [data-variant=true] select option[disabled] {
            background: #fff;
            color: #b8b8b8 !important
        }

        .ladi-story-page-progress-bar {
            width: 100%;
            height: 25px;
            position: fixed;
            top: 0;
            left: 0
        }

        .ladi-story-page-progress-bar-item {
            height: 100%;
            width: 100%;
            display: block;
            float: left;
            margin: 0 5px;
            position: relative;
            cursor: pointer
        }

        .ladi-story-page-progress-bar-item:before {
            content: '';
            position: absolute;
            background: rgba(255, 255, 255, .4);
            border-radius: 10px;
            width: 100%;
            height: 4px;
            margin: auto;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0
        }

        .ladi-story-page-progress-bar-item.active:before {
            background: #fff
        }

        .ladi-story-page-progress-bar-item span {
            background: #fff;
            border-radius: 10px;
            height: 4px;
            display: block;
            margin: auto 0;
            top: 0;
            bottom: 0;
            position: absolute;
            transition: width .3s linear;
            width: 0%
        }

        .ladi-carousel .ladi-carousel-arrow, .ladi-collection .ladi-collection-arrow, .ladi-collection .ladi-collection-button-next, .ladi-gallery .ladi-gallery-control .ladi-gallery-control-arrow, .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow, .ladi-section .ladi-section-arrow-down {
            width: 33px;
            height: 33px;
            background-repeat: no-repeat;
            background-position: center center;
            background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23000%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M7.00015%200.585938L18.4144%2012.0002L7.00015%2023.4144L5.58594%2022.0002L15.5859%2012.0002L5.58594%202.00015L7.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E")
        }

        #SECTION_POPUP .popup-close, .ladi-section .ladi-section-close, .lightbox-screen .lightbox-close {
            width: 16px;
            height: 16px;
            margin: 10px;
            background-repeat: no-repeat;
            background-position: center center;
            background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23000%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M23.4144%202.00015L2.00015%2023.4144L0.585938%2022.0002L22.0002%200.585938L23.4144%202.00015Z%22%3E%3C%2Fpath%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%200.585938L23.4144%2022.0002L22.0002%2023.4144L0.585938%202.00015L2.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E")
        }

        #BODY_BACKGROUND {
            position: fixed;
            pointer-events: none;
            top: 0;
            left: 0;
            right: 0;
            margin: 0 auto;
            height: 100vh !important
        }

        #POPUP_BLOG .ladi-headline img, #POPUP_BLOG .ladi-paragraph img {
            max-width: 100%
        }

        @media (min-width: 768px) {
            .ladi-fullwidth {
                width: 100vw !important;
                left: calc(-50vw + 50%) !important;
                box-sizing: border-box !important;
                transform: none !important
            }

            .ladi-fullwidth .ladi-gallery-view-item {
                transition-duration: 1.5s
            }
        }

        @media (max-width: 767px) {
            .ladi-element.ladi-auto-scroll {
                overflow-x: auto;
                overflow-y: hidden;
                width: 100% !important;
                left: 0 !important;
                -webkit-overflow-scrolling: touch
            }

            .ladi-section.ladi-auto-scroll {
                overflow-x: auto;
                overflow-y: hidden;
                -webkit-overflow-scrolling: touch
            }

            .ladi-carousel .ladi-carousel-content {
                transition: left .3s ease-in-out
            }

            .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item {
                transition: transform .3s ease-in-out
            }

            #POPUP_BLOG .ladi-headline img, #POPUP_BLOG .ladi-paragraph img {
                height: auto !important
            }
        }

        .ladi-notify-transition {
            transition: top .5s ease-in-out, bottom .5s ease-in-out, opacity .5s ease-in-out
        }

        .ladi-notify {
            padding: 5px;
            box-shadow: 0 0 1px rgba(64, 64, 64, .3), 0 8px 50px rgba(64, 64, 64, .05);
            border-radius: 40px;
            line-height: 1.6;
            width: 100%;
            height: 100%;
            font-size: 13px
        }

        .ladi-notify .ladi-notify-image img {
            float: left;
            margin-right: 13px;
            border-radius: 50%;
            width: 53px;
            height: 53px;
            pointer-events: none
        }

        .ladi-notify .ladi-notify-title {
            font-size: 100%;
            height: 17px;
            overflow: hidden;
            font-weight: 700;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            line-height: 1
        }

        .ladi-notify .ladi-notify-content {
            font-size: 92.308%;
            height: 17px;
            overflow: hidden;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            line-height: 1;
            padding-top: 2px
        }

        .ladi-notify .ladi-notify-time {
            line-height: 1.6;
            font-size: 84.615%;
            display: inline-block;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: calc(100% - 155px);
            overflow: hidden
        }

        .ladi-notify .ladi-notify-copyright {
            font-size: 76.9231%;
            margin-left: 2px;
            position: relative;
            padding: 0 5px;
            cursor: pointer;
            opacity: .6;
            display: inline-block;
            top: -4px
        }

        .ladi-notify .ladi-notify-copyright svg {
            vertical-align: middle
        }

        .ladi-notify .ladi-notify-copyright svg:not(:root) {
            overflow: hidden
        }

        .ladi-notify .ladi-notify-copyright div {
            text-decoration: none;
            color: rgba(64, 64, 64, 1);
            display: inline
        }

        .ladi-notify .ladi-notify-copyright strong {
            font-weight: 700
        }

        .builder-container .ladi-notify {
            transition: unset
        }

        .ladi-spin-lucky {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            box-shadow: 0 0 7px 0 rgba(64, 64, 64, .6), 0 8px 50px rgba(64, 64, 64, .3);
            background-repeat: no-repeat;
            background-size: cover
        }

        .ladi-spin-lucky .ladi-spin-lucky-start {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 20%;
            height: 20%;
            cursor: pointer;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
            transition: transform .3s ease-in-out;
            -webkit-transition: transform .3s ease-in-out
        }

        .ladi-spin-lucky .ladi-spin-lucky-start:hover {
            transform: scale(1.1)
        }

        .ladi-spin-lucky .ladi-spin-lucky-screen {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            transition: transform 7s cubic-bezier(.25, .1, 0, 1);
            -webkit-transition: transform 7s cubic-bezier(.25, .1, 0, 1);
            text-decoration-line: inherit;
            -webkit-text-decoration-line: inherit;
            text-transform: inherit
        }

        .ladi-spin-lucky .ladi-spin-lucky-screen:before {
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;
            content: '';
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none
        }

        .ladi-spin-lucky .ladi-spin-lucky-label {
            position: absolute;
            top: 50%;
            left: 50%;
            overflow: hidden;
            width: 42%;
            padding-left: 12%;
            transform-origin: 0 0;
            -webkit-transform-origin: 0 0;
            text-decoration-line: inherit;
            -webkit-text-decoration-line: inherit;
            text-transform: inherit;
            line-height: 1.6;
            text-shadow: rgba(0, 0, 0, .5) 1px 0 2px
        }</style>
    <style id="style_page" type="text/css">@media (min-width: 768px) {
            .ladi-section .ladi-container {
                width: 960px;
            }
        }

        @media (max-width: 767px) {
            .ladi-section .ladi-container {
                width: 420px;
            }
        }

        @font-face {
            font-family: "GoogleSans-Regular.ttf";
            src: url("https://w.ladicdn.com/5eba1da36b12637b2bd3f2e5/googlesans-regular-20200925070341.ttf") format("truetype");
        }

        @font-face {
            font-family: "GoogleSans-Bold.ttf";
            src: url("https://w.ladicdn.com/5eba1da36b12637b2bd3f2e5/googlesans-bold-20200925070327.ttf") format("truetype");
        }

        @font-face {
            font-family: "Montserrat-Black.ttf";
            src: url("https://w.ladicdn.com/5eba1da36b12637b2bd3f2e5/montserrat-black-20210805034113.ttf") format("truetype");
        }

        @font-face {
            font-family: "GoogleSans-Medium.ttf";
            src: url("https://w.ladicdn.com/5eba1da36b12637b2bd3f2e5/googlesans-medium-20200925070340.ttf") format("truetype");
        }

        body {
            font-family: "GoogleSans-Regular.ttf"
        }</style>
    <style id="style_element" type="text/css">@media (min-width: 768px) {
            #SECTION_POPUP {
                height: 0px;
            }

            #SECTION_POPUP .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION1 {
                height: 90px;
            }

            #SECTION1 > .ladi-section-background {
                background-color: rgb(73 221 255);
            }

            #SECTION1 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE2 {
                width: 64px;
                top: 0px;
                left: 26px;
            }

            #HEADLINE2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }
            #HEADLINE2-2 {
                width: 64px;
                top: 0px;
                left: 160px;
            }

            #HEADLINE2-2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SHAPE3 {
                width: 19.5px;
                height: 19.5px;
                top: 3.5px;
                left: 0px;
            }

            #SHAPE3 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #SHAPE3-2 {
                width: 19.5px;
                height: 19.5px;
                top: 3.5px;
                left: 134px;
            }

            #SHAPE3-2 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #GROUP4 {
                width: 90px;
                height: 27px;
                top: 30px;
                left: 0px;
            }
            #GROUP4-2 {
                width: 90px;
                height: 27px;
                top: 30px;
                right: 256px;
            }

            .logo {
                height: 90px;
                width: 90px;
                background-image: url("/web/logo.png");
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-position: center center;
                background-repeat: repeat;
                margin: 0 auto;
            }

            #SECTION5 {
                height: 61.2px;
            }

            #SECTION5 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE6 {
                width: 96px;
                top: 18.1px;
                left: 59.121px;
            }

            #HEADLINE6 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE7 {
                width: 113px;
                top: 18.1px;
                left: 182.121px;
            }

            #HEADLINE7 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE8 {
                width: 117px;
                top: 18.1px;
                left: 325px;
            }

            #HEADLINE8 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE9 {
                width: 144px;
                top: 18.1px;
                left: 476.553px;
            }

            #HEADLINE9 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE10 {
                width: 70px;
                top: 18.1px;
                left: 654px;
            }

            #HEADLINE10 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #SECTION11 {
                height: 750px;
            }

            #SECTION11 > .ladi-overlay {
                background-color: rgb(0, 0, 0);
                opacity: 0.53;
            }

            #SECTION11 > .ladi-section-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/2022-toyota-vios-thailand.jpg");
                background-position: center center;
                background-repeat: repeat;
            }

            #SECTION11 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX12 {
                width: 463.5px;
                height: 588.25px;
                top: 0px;
                left: 0px;
            }

            #BOX12 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(22, 99, 199);
                border-width: 3px;
                border-radius: 10px;
            }

            #BOX13 {
                width: 448px;
                height: 63px;
                top: 8px;
                left: 7.75px;
            }

            #BOX13 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE14 {
                width: 370px;
                top: 16.5px;
                left: 47.75px;
            }

            #HEADLINE14 > .ladi-headline {
                font-family: "GoogleSans-Bold.ttf";
                color: rgb(255, 255, 255);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM15 {
                width: 426px;
                height: 423px;
                top: 96px;
                left: 19.75px;
            }

            #FORM15 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM15 .ladi-form-item .ladi-form-control::placeholder, #FORM15 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM15 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM15 .ladi-form-item-container, #FORM15 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM15 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #BUTTON16-16 {
                width: 272.086px;
                height: 39.9483px;
                top: 427px;
                left: 76.957px;
            }

            #BUTTON16-16 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #BUTTON_TEXT16-16 {
                width: 232px;
                top: 10.2724px;
                left: 0px;
            }

            #BUTTON_TEXT16-16 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM_ITEM17 {
                width: 426px;
                height: 39.9483px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM19 {
                width: 426px;
                height: 39.9483px;
                top: 52.724px;
                left: 0px;
            }

            #FORM_ITEM20 {
                width: 426px;
                height: 114.138px;
                top: 295.086px;
                left: 0px;
            }

            #FORM_ITEM21 {
                width: 426px;
                height: 39.9483px;
                top: 101.724px;
                left: 0px;
            }

            #FORM_ITEM22 {
                width: 426px;
                height: 40px;
                top: 154.224px;
                left: 0px;
            }

            #FORM_ITEM23 {
                width: 426px;
                height: 40px;
                top: 201.086px;
                left: 0px;
                position: relative;
                display: inline-flex;
            }

            #FORM_ITEM24 {
                width: 426px;
                height: 40px;
                top: 248.086px;
                left: 0px;
            }

            #GROUP24 {
                width: 463.5px;
                height: 551.25px;
                top: 41.6px;
                left: -63.5px;
            }

            #LIST_PARAGRAPH27 {
                width: 424px;
                top: 511.85px;
                left: 548.931px;
            }

            #LIST_PARAGRAPH27 > .ladi-list-paragraph {
                color: rgb(255, 255, 255);
                font-size: 19px;
                font-weight: bold;
                line-height: 1.6;
            }

            #LIST_PARAGRAPH27 ul li {
                padding-left: 20px;
            }

            #LIST_PARAGRAPH27 ul li:before {
                content: "";
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22100%25%22%20height%3D%22100%25%22%20%20viewBox%3D%220%200%201536%201896.0833%22%20class%3D%22%22%20fill%3D%22rgba(255%2C%20255%2C%20255%2C%201)%22%3E%20%3Cpath%20d%3D%22M1171%20813l-422%20422q-19%2019-45%2019t-45-19L365%20941q-19-19-19-45t19-45l102-102q19-19%2045-19t45%2019l147%20147%20275-275q19-19%2045-19t45%2019l102%20102q19%2019%2019%2045t-19%2045zm141%2083q0-148-73-273t-198-198-273-73-273%2073-198%20198-73%20273%2073%20273%20198%20198%20273%2073%20273-73%20198-198%2073-273zm224%200q0%20209-103%20385.5T1153.5%201561%20768%201664t-385.5-103T103%201281.5%200%20896t103-385.5T382.5%20231%20768%20128t385.5%20103T1433%20510.5%201536%20896z%22%3E%3C%2Fpath%3E%20%3C%2Fsvg%3E");
                width: 15px;
                height: 15px;
                top: 8px;
            }

            #SECTION28 {
                height: 600px;
            }

            #SECTION28 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE29 {
                width: 431px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE29 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 30px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BOX30 {
                width: 139px;
                height: 5px;
                top: 93px;
                left: 146px;
            }

            #BOX30 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #GROUP31 {
                width: 431px;
                height: 98px;
                top: 20.4px;
                left: 264.5px;
            }

            #IMAGE32 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 69.5px;
            }

            #IMAGE32 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/piggy-bank-20211104091147.png");
            }

            #HEADLINE33 {
                width: 133px;
                top: 101px;
                left: 47px;
            }

            #HEADLINE33 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 19px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE34 {
                width: 223px;
                top: 133.2px;
                left: 0px;
            }

            #HEADLINE34 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP35 {
                width: 223px;
                height: 184.2px;
                top: 152.15px;
                left: -6px;
            }

            #IMAGE37 {
                width: 78px;
                height: 78px;
                top: 0px;
                left: 73.5px;
            }

            #IMAGE37 > .ladi-image > .ladi-image-background {
                width: 78px;
                height: 78px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/car-20211104091147.png");
            }

            #HEADLINE38 {
                width: 133px;
                top: 95px;
                left: 47px;
            }

            #HEADLINE38 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 19px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE39 {
                width: 223px;
                top: 127.2px;
                left: 0px;
            }

            #HEADLINE39 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP36 {
                width: 223px;
                height: 178.2px;
                top: 157.4px;
                left: 254.667px;
            }

            #IMAGE41 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 81.5px;
            }

            #IMAGE41 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/rate-20211104091147.png");
            }

            #HEADLINE42 {
                width: 149px;
                top: 101px;
                left: 49px;
            }

            #HEADLINE42 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 19px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE43 {
                width: 247px;
                top: 132.7px;
                left: 0px;
            }

            #HEADLINE43 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP40 {
                width: 247px;
                height: 158.7px;
                top: 151.15px;
                left: 494.833px;
            }

            #IMAGE45 {
                width: 77px;
                height: 77px;
                top: 0px;
                left: 64.5px;
            }

            #IMAGE45 > .ladi-image > .ladi-image-background {
                width: 77px;
                height: 77px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/shield-20211104091147.png");
            }

            #HEADLINE46 {
                width: 182px;
                top: 94px;
                left: 12px;
            }

            #HEADLINE46 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 19px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE47 {
                width: 206px;
                top: 124.7px;
                left: 0px;
            }

            #HEADLINE47 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP44 {
                width: 206px;
                height: 175.7px;
                top: 156.15px;
                left: 754px;
            }

            #HEADLINE48 {
                width: 878px;
                top: 397.9px;
                left: 46.793px;
            }

            #HEADLINE48 > .ladi-headline {
                color: rgb(222 180 71);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #LINE49 {
                width: 878px;
                top: 366.9px;
                left: 46.793px;
            }

            #LINE49 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(73 221 255);
                border-right: 2px solid rgb(73 221 255);
                border-bottom: 2px solid rgb(73 221 255);
                border-left: 0px !important;
            }

            #LINE49 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #LINE50 {
                width: 878px;
                top: 892px;
                left: 46.793px;
            }

            #LINE50 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(73 221 255);
                border-right: 2px solid rgb(73 221 255);
                border-bottom: 2px solid rgb(73 221 255);
                border-left: 0px !important;
            }

            #LINE50 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #SECTION51 {
                height: 390.2px;
            }

            #SECTION51 > .ladi-section-background {
                background-color: rgba(228, 228, 228, 0.5);
            }

            #SECTION51 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION55 {
                height: 1000px;
            }

            #SECTION55 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION120 {
                height: 336.2px;
            }

            #SECTION120 > .ladi-overlay {
                background-color: rgb(4, 15, 40);
                opacity: 0.95;
            }

            #SECTION120 > .ladi-section-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/2022-toyota-vios-thailand.jpg");
                background-position: center bottom;
                background-repeat: repeat;
            }

            #SECTION120 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE185 {
                width: 306px;
                top: 47.2px;
                left: 41px;
            }

            #HEADLINE185 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                text-decoration-line: underline;
                -webkit-text-decoration-line: underline;
                color: rgb(255, 255, 255);
                font-size: 25px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: left;
                line-height: 1.4;
            }

            #LINE219 {
                width: 960px;
                top: 279.03px;
                left: 0px;
            }

            #LINE219 > .ladi-line > .ladi-line-container {
                border-top: 1px solid rgba(255, 255, 255, 0.1);
                border-right: 1px solid rgba(255, 255, 255, 0.1);
                border-bottom: 1px solid rgba(255, 255, 255, 0.1);
                border-left: 0px !important;
            }

            #LINE219 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE220 {
                width: 391px;
                top: 303.03px;
                left: 300.066px;
            }

            #HEADLINE220 > .ladi-headline {
                color: rgba(255, 255, 255, 0.9);
                font-size: 12px;
                text-align: center;
                line-height: 1.2;
            }

            #SHAPE227 {
                width: 22.5469px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE227 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE228 {
                width: 243px;
                top: 2px;
                left: 33px;
            }

            #HEADLINE228 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP226 {
                width: 276px;
                height: 22.5469px;
                top: 90px;
                left: 44.0658px;
            }

            #SHAPE233 {
                width: 28.7993px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE233 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE234 {
                width: 292px;
                top: 3px;
                left: 35px;
            }

            #HEADLINE234 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP232 {
                width: 327px;
                height: 22.5469px;
                top: 164.403px;
                left: 41px;
            }

            #IMAGE235 {
                width: 344px;
                height: 102.381px;
                top: 570px;
                left: 313.793px;
            }

            #IMAGE235 > .ladi-image > .ladi-image-background {
                width: 344px;
                height: 102.381px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/5eba1da36b12637b2bd3f2e5/hotline-20211104103042.gif");
            }

            #POPUP245 {
                width: 420px;
                height: 498px;
                top: 0px;
                left: 0px;
                right: 0px;
                bottom: 0px;
                margin: auto;
            }

            #POPUP245 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #POPUP245 .popup-close {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M23.4144%202.00015L2.00015%2023.4144L0.585938%2022.0002L22.0002%200.585938L23.4144%202.00015Z%22%3E%3C%2Fpath%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%200.585938L23.4144%2022.0002L22.0002%2023.4144L0.585938%202.00015L2.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX247 {
                width: 420px;
                height: 499.515px;
                top: 0px;
                left: 0px;
            }

            #BOX247 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(22, 99, 199);
                border-width: 3px;
                border-radius: 10px;
            }

            #BOX248 {
                width: 405.955px;
                height: 57.0874px;
                top: 7.2492px;
                left: 7.02265px;
            }

            #BOX248 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE249 {
                width: 335px;
                top: 14.9515px;
                left: 43.2686px;
            }

            #HEADLINE249 > .ladi-headline {
                font-family: "GoogleSans-Bold.ttf";
                color: rgb(255, 255, 255);
                font-size: 28px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON_TEXT251 {
                width: 247px;
                top: 9.30832px;
                left: 0px;
            }

            #BUTTON_TEXT251 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON251 {
                width: 246.55px;
                height: 36.1991px;
                top: 347.102px;
                left: 69.7344px;
            }

            #BUTTON251 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #FORM_ITEM253 {
                width: 386.019px;
                height: 36.1991px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM254 {
                width: 386.019px;
                height: 36.1991px;
                top: 47.7758px;
                left: 0px;
            }

            #FORM_ITEM255 {
                width: 386.019px;
                height: 103.426px;
                top: 229.334px;
                left: 0px;
            }

            #FORM_ITEM256 {
                width: 386.019px;
                height: 36.1991px;
                top: 92.1771px;
                left: 0px;
            }

            #FORM_ITEM257 {
                width: 386.019px;
                height: 36.246px;
                top: 139.75px;
                left: 0px;
            }

            #FORM_ITEM258 {
                width: 386.019px;
                height: 36.246px;
                top: 182.214px;
                left: 0px;
            }

            #FORM250 {
                width: 386.019px;
                height: 383.301px;
                top: 86.9904px;
                left: 17.8964px;
            }

            #FORM250 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM250 .ladi-form-item .ladi-form-control::placeholder, #FORM250 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM250 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM250 .ladi-form-item-container, #FORM250 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM250 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #GROUP246 {
                width: 420px;
                height: 499.515px;
                top: 0px;
                left: 0px;
            }

            #IMAGE259 {
                width: 587.165px;
                height: 383.726px;
                left: 426.873px;
            }

            #IMAGE259 > .ladi-image > .ladi-image-background {
                width: 836.538px;
                height: 557.389px;
                top: -124.463px;
                left: -132.088px;
                background-image: url("/web/xpander.png");
            }

            #IMAGE259.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE260 {
                width: 136px;
                top: 18.1px;
                left: 772px;
            }

            #HEADLINE260 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE261 {
                width: 621px;
                top: 401.726px;
                left: 461.931px;
            }

            #HEADLINE261 > .ladi-headline {
                font-family: "Montserrat-Black.ttf";
                color: rgb(255, 255, 255);
                font-size: 30px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #HEADLINE261 > .nha-xe {
                font-family: "Montserrat-Black.ttf";
                font-size: 40px;
                font-weight: bold;
                text-transform: uppercase;
                letter-spacing: 1px;
                line-height: 1.4;
                color: rgb(38 187 43);
                text-align: center;
            }

            #HEADLINE288 {
                width: 770px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE288 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 30px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BOX289 {
                width: 139px;
                height: 5px;
                top: 57px;
                left: 315.5px;
            }

            #BOX289 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #GROUP287 {
                width: 770px;
                height: 62px;
                top: 27.3px;
                left: 95px;
            }

            #SHAPE309 {
                width: 22.7993px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE309 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE310 {
                width: 374px;
                top: 3px;
                left: 33px;
            }

            #HEADLINE310 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP308 {
                width: 407px;
                height: 23px;
                top: 132.403px;
                left: 43px;
            }

            #BUTTON_TEXT312 {
                width: 232px;
                top: 10.2724px;
                left: 0px;
            }

            #BUTTON_TEXT312 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON312 {
                width: 198.041px;
                height: 39.9482px;
                top: 160.328px;
                left: 84.9795px;
            }

            #BUTTON312 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #FORM_ITEM315 {
                width: 368px;
                height: 39.9482px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM317 {
                width: 368px;
                height: 39.9482px;
                top: 48.9999px;
                left: 0px;
            }

            #FORM_ITEM318 {
                width: 368px;
                height: 39.9999px;
                top: 101.5px;
                left: 0px;
            }

            #FORM311 {
                width: 368px;
                height: 200.276px;
                top: 43.5383px;
                left: 380px;
            }

            #FORM311 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM311 .ladi-form-item .ladi-form-control::placeholder, #FORM311 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM311 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM311 .ladi-form-item-container, #FORM311 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM311 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #FORM311 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
                border-radius: 4px
            }

            #FORM312 {
                width: 368px;
                height: 200.276px;
                top: 43.5383px;
                left: 795px;
            }

            #FORM312 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM312 .ladi-form-item .ladi-form-control::placeholder, #FORM312 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM312 .ladi-form-item-container, #FORM312 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM312 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #FORM312 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
                border-radius: 4px
            }

            #CAROUSEL326 {
                width: 960px;
                height: 250.349px;
                bottom: 40px;
                left: 0px;
            }

            #CAROUSEL326 .ladi-carousel .ladi-carousel-arrow {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M7.00015%200.585938L18.4144%2012.0002L7.00015%2023.4144L5.58594%2022.0002L15.5859%2012.0002L5.58594%202.00015L7.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #IMAGE327 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 6px;
            }

            #IMAGE327 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car1.jpeg");
            }

            #IMAGE328 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 257px;
            }

            #IMAGE328 > .ladi-image > .ladi-image-background {
                width: 364.39px;
                height: 238px;
                top: 0px;
                left: -59.024px;
                background-image: url("/web/images/s700x550/5eba1da36b12637b2bd3f2e5/gia-xe-toyota-innova-2020-oto-co-20220802064747.png");
            }

            #IMAGE329 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 506px;
            }

            #IMAGE329 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car2.jpeg");
            }

            #IMAGE330 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 756px;
            }

            #IMAGE330 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car3.jpeg");
            }

            #IMAGE331 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 1005px;
            }

            #IMAGE331 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s550x550/5eba1da36b12637b2bd3f2e5/xe-san-bay-airport2-653x440-2022-20220802064748.png");
            }

            #IMAGE332 {
                width: 472.062px;
                height: 265.474px;
                top: 111.563px;
                left: 0px;
            }

            #IMAGE332 > .ladi-image > .ladi-image-background {
                width: 472.062px;
                height: 265.474px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s800x600/5eba1da36b12637b2bd3f2e5/123-20220413161251-20220802074926.png");
            }

            #IMAGE333 {
                width: 472.062px;
                height: 265.474px;
                top: 111.563px;
                left: 487.938px;
            }

            #IMAGE333 > .ladi-image > .ladi-image-background {
                width: 472.062px;
                height: 265.474px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s800x600/5eba1da36b12637b2bd3f2e5/maxresdefault-20220413161251-20220802074926.png");
            }

            #HEADLINE334 {
                width: 119px;
                top: 30px;
                left: 91.121px;
            }

            #HEADLINE334 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }
            #hot-line {
                width: 119px;
                top: 30px;
                right: 91.121px;
            }
            #hot-line > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }
            #hot-line-2 {
                width: 119px;
                top: 30px;
                right: 1px;
            }
            #hot-line-2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE335 {
                width: 137px;
                top: 4.6px;
                left: 197px;
            }

            #HEADLINE335 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE344 {
                width: 169px;
                top: 15.1295px;
                left: 70.0441px;
            }

            #HEADLINE344 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 29px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE344.ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #IMAGE343 {
                width: 62.9147px;
                height: 75.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE343 > .ladi-image > .ladi-image-background {
                width: 62.9147px;
                height: 75.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE343.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BOX342 {
                width: 194.886px;
                height: 46px;
                top: 15.1295px;
                left: 57.1011px;
            }

            #BOX342 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }

            #GROUP341 {
                width: 251.987px;
                height: 75.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 78px;
                position: fixed;
                z-index: 90000050;
            }

            #GROUP305 {
                width: 770px;
                height: 62px;
                top: 28.1px;
                left: 94.996px;
            }

            #BOX307 {
                width: 139px;
                height: 5px;
                top: 57px;
                left: 315.5px;
            }

            #BOX307 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE306 {
                width: 770px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE306 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 30px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #IMAGE236 {
                width: 344px;
                height: 102.381px;
                top: 497.91px;
                left: 307.996px;
            }

            #IMAGE236 > .ladi-image > .ladi-image-background {
                width: 344px;
                height: 102.381px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/5eba1da36b12637b2bd3f2e5/hotline-20211104103042.gif");
            }

            #GROUP175 {
                width: 270px;
                height: 350.248px;
                top: 128.976px;
                left: -95px;
            }

            #PARAGRAPH184 {
                width: 270px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH184 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE183 {
                width: 138px;
                top: 136.719px;
                left: 67.25px;
            }

            #HEADLINE183 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP177 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 90.8858px;
            }

            #SHAPE182 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE182 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE181 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE181 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE180 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE180 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE179 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE179 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE178 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE178 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE176 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 72.65px;
            }

            #IMAGE176 > .ladi-image > .ladi-image-background {
                width: 143.7px;
                height: 206.013px;
                top: -82px;
                left: -19px;
                background-image: url("/web/customers/1.jpeg");
            }

            #IMAGE176 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP165 {
                width: 270px;
                height: 298.248px;
                top: 128.976px;
                left: 498.333px;
            }

            #PARAGRAPH174 {
                width: 270px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH174 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE173 {
                width: 180px;
                top: 137.124px;
                left: 45px;
            }

            #HEADLINE173 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP167 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 90.8858px;
            }

            #SHAPE172 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE172 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE171 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE171 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE170 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE170 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE169 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE169 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE168 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE168 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE166 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 72.65px;
            }

            #IMAGE166 > .ladi-image > .ladi-image-background {
                width: 124.7px;
                height: 124.7px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s450x450/5eba1da36b12637b2bd3f2e5/8206116b260ba5f42174b7c24286798d-20211104101342.jpg");
            }

            #IMAGE166 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP155 {
                width: 270px;
                height: 298.248px;
                top: 128.976px;
                left: 201.667px;
            }

            #PARAGRAPH164 {
                width: 270px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH164 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE163 {
                width: 138px;
                top: 136.719px;
                left: 67.25px;
            }

            #HEADLINE163 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP157 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 90.8858px;
            }

            #SHAPE162 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE162 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE161 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE161 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE160 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE160 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE159 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE159 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE158 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE158 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE156 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 72.65px;
            }

            #IMAGE156 > .ladi-image > .ladi-image-background {
                width: 172.075px;
                height: 143.677px;
                top: -2px;
                left: -12.3503px;
                background-image: url("/web/images/s500x450/5eba1da36b12637b2bd3f2e5/chi-lan--20211111025719.jpg");
            }

            #IMAGE156 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP154 {
                width: 270px;
                height: 324.248px;
                top: 128.976px;
                left: 795px;
            }

            #PARAGRAPH153 {
                width: 270px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH153 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE151 {
                width: 138px;
                top: 136.719px;
                left: 67.25px;
            }

            #HEADLINE151 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP145 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 90.8858px;
            }

            #SHAPE150 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE150 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE149 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE149 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE148 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE148 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE147 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE147 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE146 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE146 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE142 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 72.65px;
            }

            #IMAGE142 > .ladi-image > .ladi-image-background {
                width: 154.7px;
                height: 124.7px;
                top: 0px;
                left: -30px;
                background-image: url("/web/customers/2.jpeg");
            }

            #IMAGE142 > .ladi-image {
                border-radius: 123px;
            }

            #SECTION97 {
                height: 620.9px;
            }

            #SECTION97 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX346 {
                width: 154.004px;
                height: 26.9724px;
                top: 14.7661px;
                left: 38.4628px;
            }

            #BOX346 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }
            #BOX346-2 {
                width: 154.004px;
                height: 26.9724px;
                top: 14.7661px;
                left: 38.4628px;
            }

            #BOX346-2 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }

            #IMAGE347 {
                width: 52.3507px;
                height: 56.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE347 > .ladi-image > .ladi-image-background {
                width: 52.3507px;
                height: 56.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE347.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #IMAGE347-2 {
                width: 52.3507px;
                height: 56.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE347-2 > .ladi-image > .ladi-image-background {
                width: 52.3507px;
                height: 56.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE347-2.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #HEADLINE348 {
                width: 133px;
                top: 10.7527px;
                left: 55.3507px;
            }

            #HEADLINE348 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 22px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE348.ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE348-2 {
                width: 133px;
                top: 10.7527px;
                left: 55.3507px;
            }

            #HEADLINE348-2 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 22px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE348-2 .ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #GROUP345 {
                width: 192.467px;
                height: 56.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 12px;
                position: fixed;
                z-index: 90000050;
            }
            #GROUP345-2 {
                width: 192.467px;
                height: 56.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 85px;
                position: fixed;
                z-index: 90000050;
            }

            #BOX367 {
                width: 71.9998px;
                height: 71.9998px;
                top: auto;
                left: 15px;
                right: auto;
                bottom: 165px;
                position: fixed;
                z-index: 90000050;
            }

            #BOX367 > .ladi-box {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/images/s400x400/5c7362c6c417ab07e5196b05/6aecd4d3a513e32a11391a56d260574b-1-20200424042407-20200706022304.png");
                background-position: center top;
                background-repeat: repeat;
                border-radius: 0px;
            }

            #BOX367.ladi-animation > .ladi-box {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }
        }
        #location ol {
            margin: auto;
            padding: 15px 50px;
            width: 50%;
        }
        #location ol li {
            margin: 15px;
            background-color: rgb(73 221 255);
            padding: 10px 8px;
            border: 1px solid rgb(6 132 161);
            border-radius: 5px;
        }

        @media (max-width: 767px) {
            #location ol {
                margin: auto;
                padding: 15px 50px;
                width: 100%;
            }
            #location ol li {
                margin: 15px 5px;
                background-color: rgb(73 221 255);
                padding: 10px 5px;
                border: 1px solid rgb(6 132 161);
                border-radius: 5px;
            }
            #SECTION_POPUP {
                height: 0px;
            }

            #SECTION_POPUP .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION1 {
                height: 85px;
            }

            #SECTION1 > .ladi-section-background {
                background-color: rgb(73 221 255);
            }

            #SECTION1 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE2 {
                width: 67px;
                top: 0px;
                left: 26px;
            }

            #HEADLINE2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE2-2 {
                width: 67px;
                top: 0px;
                left: 26px;
            }

            #HEADLINE2-2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SHAPE3 {
                width: 19.5px;
                height: 19.5px;
                top: 3.5px;
                left: 8px;
            }

            #SHAPE3 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #SHAPE3-2 {
                width: 19.5px;
                height: 19.5px;
                top: 3.5px;
                left: 8px;
            }

            #SHAPE3-2 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #GROUP4 {
                width: 93px;
                height: 27px;
                top: 12.5px;
                right: 140px;
            }
            #GROUP4-2 {
                width: 93px;
                height: 27px;
                top: 42.5px;
                right: 140px;
            }

            /*#SECTION5 {*/
            /*    height: 18px;*/
            /*}*/
            #SECTION5 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE6 {
                width: 96px;
                top: 6px;
                left: 430.5px;
            }

            #HEADLINE6 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE7 {
                width: 131px;
                top: 6px;
                left: 441.5px;
            }

            #HEADLINE7 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE8 {
                width: 103px;
                top: 6px;
                left: 430.5px;
            }

            #HEADLINE8 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE9 {
                width: 148px;
                top: 6px;
                left: 447px;
            }

            #HEADLINE9 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE10 {
                width: 70px;
                top: 6px;
                left: 447px;
            }

            #HEADLINE10 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #SECTION11 {
                height: 945.81px;
            }

            #SECTION11 > .ladi-overlay {
                background-color: rgb(0, 0, 0);
                opacity: 0.53;
            }

            #SECTION11 > .ladi-section-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/2022-toyota-vios-thailand.jpg");
                background-position: center center;
                background-repeat: repeat;
            }

            #SECTION11 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX12 {
                width: 395.75px;
                height: 564.25px;
                top: 0px;
                left: 0px;
            }

            #BOX12 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(22, 99, 199);
                border-width: 3px;
                border-radius: 10px;
            }

            #BOX13 {
                width: 382.516px;
                height: 63px;
                top: 8px;
                left: 6.61718px;
            }

            #BOX13 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE14 {
                width: 392px;
                top: 19.5px;
                left: 2.7291px;
            }

            #HEADLINE14 > .ladi-headline {
                font-family: "GoogleSans-Bold.ttf";
                color: rgb(255, 255, 255);
                font-size: 25px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM15 {
                width: 363.732px;
                height: 423px;
                top: 96px;
                left: 16.8631px;
            }

            #FORM15 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM15 .ladi-form-item .ladi-form-control::placeholder, #FORM15 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM15 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM15 .ladi-form-item-container, #FORM15 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM15 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #BUTTON16-16 {
                width: 232.315px;
                height: 39.9483px;
                top: 420.052px;
                left: 65.7082px;
            }

            #BUTTON16-16 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #BUTTON_TEXT16-16 {
                width: 272px;
                top: 10.2724px;
                left: 0px;
            }

            #BUTTON_TEXT16-16 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM_ITEM17 {
                width: 363.732px;
                height: 39.9483px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM19 {
                width: 363.732px;
                height: 39.9483px;
                top: 52.724px;
                left: 0px;
            }

            #FORM_ITEM20 {
                width: 363.732px;
                height: 114.138px;
                top: 298.086px;
                left: 0px;
            }

            #FORM_ITEM21 {
                width: 363.732px;
                height: 39.9483px;
                top: 101.724px;
                left: 0px;
            }

            #FORM_ITEM22 {
                width: 363.732px;
                height: 40px;
                top: 154.224px;
                left: 0px;
            }

            #FORM_ITEM23 {
                width: 363.732px;
                height: 40px;
                top: 201.086px;
                left: 0px;
                position: relative;
                display: inline-flex;
            }

            #FORM_ITEM24 {
                width: 363.732px;
                height: 39.9483px;
                top: 250.724px;
                left: 0px;
            }

            #GROUP24 {
                width: 395.75px;
                height: 551.25px;
                top: 375.558px;
                left: 12.125px;
            }

            #LIST_PARAGRAPH27 {
                width: 380px;
                top: 251.558px;
                left: 27.875px;
            }

            #LIST_PARAGRAPH27 > .ladi-list-paragraph {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
                margin-top: 10px;
            }

            #LIST_PARAGRAPH27 ul li {
                padding-left: 20px;
            }

            #LIST_PARAGRAPH27 ul li:before {
                content: "";
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22100%25%22%20height%3D%22100%25%22%20%20viewBox%3D%220%200%201536%201896.0833%22%20class%3D%22%22%20fill%3D%22rgba(255%2C%20255%2C%20255%2C%201)%22%3E%20%3Cpath%20d%3D%22M1171%20813l-422%20422q-19%2019-45%2019t-45-19L365%20941q-19-19-19-45t19-45l102-102q19-19%2045-19t45%2019l147%20147%20275-275q19-19%2045-19t45%2019l102%20102q19%2019%2019%2045t-19%2045zm141%2083q0-148-73-273t-198-198-273-73-273%2073-198%20198-73%20273%2073%20273%20198%20198%20273%2073%20273-73%20198-198%2073-273zm224%200q0%20209-103%20385.5T1153.5%201561%20768%201664t-385.5-103T103%201281.5%200%20896t103-385.5T382.5%20231%20768%20128t385.5%20103T1433%20510.5%201536%20896z%22%3E%3C%2Fpath%3E%20%3C%2Fsvg%3E");
                width: 15px;
                height: 15px;
                top: 8px;
            }

            #SECTION28 {
                height: 1300px;
            }

            #SECTION28 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE29 {
                width: 409px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE29 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 25px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BOX30 {
                width: 148.049px;
                height: 5px;
                top: 79px;
                left: 130.475px;
            }

            #BOX30 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #GROUP31 {
                width: 409px;
                height: 84px;
                top: 21px;
                left: 5.5px;
            }

            #IMAGE32 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 96px;
            }

            #IMAGE32 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/piggy-bank-20211104091147.png");
            }

            #HEADLINE33 {
                width: 133px;
                top: 101px;
                left: 73.5px;
            }

            #HEADLINE33 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 17px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE34 {
                width: 280px;
                top: 133.2px;
                left: 0px;
            }

            #HEADLINE34 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP35 {
                width: 280px;
                height: 183.2px;
                top: 128px;
                left: 70px;
            }

            #IMAGE37 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 116.5px;
            }

            #IMAGE37 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/car-20211104091147.png");
            }

            #HEADLINE38 {
                width: 133px;
                top: 101px;
                left: 94px;
            }

            #HEADLINE38 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 17px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE39 {
                width: 321px;
                top: 133.2px;
                left: 0px;
            }

            #HEADLINE39 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP36 {
                width: 321px;
                height: 158.2px;
                top: 332.6px;
                left: 49.5px;
            }

            #IMAGE41 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 81.5px;
            }

            #IMAGE41 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/rate-20211104091147.png");
            }

            #HEADLINE42 {
                width: 149px;
                top: 101px;
                left: 49px;
            }

            #HEADLINE42 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 17px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE43 {
                width: 247px;
                top: 132.7px;
                left: 0px;
            }

            #HEADLINE43 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP40 {
                width: 247px;
                height: 157.7px;
                top: 508.2px;
                left: 86.5px;
            }

            #IMAGE45 {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 106px;
            }

            #IMAGE45 > .ladi-image > .ladi-image-background {
                width: 84px;
                height: 84px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s400x400/5eba1da36b12637b2bd3f2e5/shield-20211104091147.png");
            }

            #HEADLINE46 {
                width: 182px;
                top: 101px;
                left: 59px;
            }

            #HEADLINE46 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 17px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #HEADLINE47 {
                width: 300px;
                top: 131.7px;
                left: 0px;
            }

            #HEADLINE47 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP44 {
                width: 300px;
                height: 156.7px;
                top: 684.9px;
                left: 60px;
            }

            #HEADLINE48 {
                width: 400px;
                top: 890.6px;
                left: 10px;
            }

            #HEADLINE48 > .ladi-headline {
                color: rgb(222 180 71);
                font-size: 17px;
                line-height: 1.6;
            }

            #LINE49 {
                width: 400px;
                top: 862.6px;
                left: 10px;
            }

            #LINE49 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(73 221 255);
                border-right: 2px solid rgb(73 221 255);
                border-bottom: 2px solid rgb(73 221 255);
                border-left: 0px !important;
            }

            #LINE49 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #LINE50 {
                width: 400px;
                top: 1540px;
                left: 10px;
            }

            #LINE50 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(73 221 255);
                border-right: 2px solid rgb(73 221 255);
                border-bottom: 2px solid rgb(73 221 255);
                border-left: 0px !important;
            }

            #LINE50 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #SECTION51 {
                height: 325.949px;
            }

            #SECTION51 > .ladi-section-background {
                background-color: rgba(228, 228, 228, 0.5);
            }

            #SECTION51 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION55 {
                height: 1000px;
            }

            #SECTION55 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #SECTION120 {
                height: 735px;
            }

            #SECTION120 > .ladi-overlay {
                background-color: rgb(4, 15, 40);
                opacity: 0.95;
            }

            #SECTION120 > .ladi-section-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/images/s768x483/5eba1da36b12637b2bd3f2e5/ny2-505-1-20211111013319.jpg");
                background-position: center bottom;
                background-repeat: repeat;
            }

            #SECTION120 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #HEADLINE185 {
                width: 365px;
                top: 31.31px;
                left: 27.951px;
            }

            #HEADLINE185 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                text-decoration-line: underline;
                -webkit-text-decoration-line: underline;
                color: rgb(255, 255, 255);
                font-size: 30px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: left;
                line-height: 1.4;
            }

            #LINE219 {
                width: 400px;
                top: 415.951px;
                left: 9.817px;
            }

            #LINE219 > .ladi-line > .ladi-line-container {
                border-top: 1px solid rgba(255, 255, 255, 0.1);
                border-right: 1px solid rgba(255, 255, 255, 0.1);
                border-bottom: 1px solid rgba(255, 255, 255, 0.1);
                border-left: 0px !important;
            }

            #LINE219 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE220 {
                width: 391px;
                bottom: 30px;
                left: 18.817px;
            }

            #HEADLINE220 > .ladi-headline {
                color: rgba(255, 255, 255, 0.9);
                font-size: 12px;
                text-align: center;
                line-height: 1.2;
            }

            #SHAPE227 {
                width: 22.5469px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE227 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE228 {
                width: 243px;
                top: 2px;
                left: 33px;
            }

            #HEADLINE228 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP226 {
                width: 276px;
                height: 22.5469px;
                top: 92.31px;
                left: 27.317px;
            }

            #SHAPE233 {
                width: 28.7993px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE233 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE234 {
                width: 292px;
                top: 3px;
                left: 35px;
            }

            #HEADLINE234 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP232 {
                width: 327px;
                height: 22.5469px;
                top: 160.404px;
                left: 22.817px;
            }

            #IMAGE235 {
                width: 344px;
                height: 102.381px;
                top: 570px;
                left: 38px;
            }

            #IMAGE235 > .ladi-image > .ladi-image-background {
                width: 344px;
                height: 102.381px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/5eba1da36b12637b2bd3f2e5/hotline-20211104103042.gif");
            }

            #POPUP245 {
                width: 366.024px;
                height: 434px;
                top: 0px;
                left: 0px;
                right: 0px;
                bottom: 0px;
                margin: auto;
            }

            #POPUP245 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #POPUP245 .popup-close {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M23.4144%202.00015L2.00015%2023.4144L0.585938%2022.0002L22.0002%200.585938L23.4144%202.00015Z%22%3E%3C%2Fpath%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%200.585938L23.4144%2022.0002L22.0002%2023.4144L0.585938%202.00015L2.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX247 {
                width: 366.024px;
                height: 435.32px;
                top: 0px;
                left: 0px;
            }

            #BOX247 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(22, 99, 199);
                border-width: 3px;
                border-radius: 10px;
            }

            #BOX248 {
                width: 353.784px;
                height: 49.7508px;
                top: 6.31757px;
                left: 6.12014px;
            }

            #BOX248 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE249 {
                width: 292px;
                top: 13.03px;
                left: 37.708px;
            }

            #HEADLINE249 > .ladi-headline {
                font-family: "GoogleSans-Bold.ttf";
                color: rgb(255, 255, 255);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON_TEXT251 {
                width: 215px;
                top: 8.11207px;
                left: 0px;
            }

            #BUTTON_TEXT251 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON251 {
                width: 214.865px;
                height: 31.547px;
                top: 302.494px;
                left: 60.7725px;
            }

            #BUTTON251 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #FORM_ITEM253 {
                width: 336.41px;
                height: 31.547px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM254 {
                width: 336.41px;
                height: 31.547px;
                top: 41.6359px;
                left: 0px;
            }

            #FORM_ITEM255 {
                width: 336.41px;
                height: 90.1342px;
                top: 199.861px;
                left: 0px;
            }

            #FORM_ITEM256 {
                width: 336.41px;
                height: 31.547px;
                top: 80.3309px;
                left: 0px;
            }

            #FORM_ITEM257 {
                width: 336.41px;
                height: 31.5878px;
                top: 121.79px;
                left: 0px;
            }

            #FORM_ITEM258 {
                width: 336.41px;
                height: 31.5878px;
                top: 158.797px;
                left: 0px;
            }

            #FORM250 {
                width: 336.41px;
                height: 334.041px;
                top: 75.8109px;
                left: 15.5965px;
            }

            #FORM250 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM250 .ladi-form-item .ladi-form-control::placeholder, #FORM250 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM250 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM250 .ladi-form-item-container, #FORM250 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM250 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #GROUP246 {
                width: 366.024px;
                height: 435.32px;
                top: 0px;
                left: 0px;
            }

            #IMAGE259 {
                width: 400px;
                height: 179px;
                top: 7.808px;
                left: 10px;
            }

            #IMAGE259 > .ladi-image > .ladi-image-background {
                width: 328px;
                height: 251.522px;
                top: -33px;
                left: 23px;
                background-image: url("/web/images/kia-sedona.png");
            }

            #IMAGE259.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE260 {
                width: 260px;
                top: 6px;
                left: 460px;
            }

            #HEADLINE260 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 1px;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.6;
            }

            #HEADLINE261 {
                width: 409px;
                top: 192px;
                left: 5.5px;
            }

            #HEADLINE261 > .ladi-headline {
                font-family: "Montserrat-Black.ttf";
                color: rgb(255, 255, 255);
                font-size: 18px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #HEADLINE261 > .nha-xe {
                font-family: "Montserrat-Black.ttf";
                font-size: 35px;
                font-weight: bold;
                text-transform: uppercase;
                letter-spacing: 1px;
                color: rgb(38 187 43);
                text-align: center;
                padding-bottom: 5px;
            }

            #HEADLINE288 {
                width: 409px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE288 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 25px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BOX289 {
                width: 148.049px;
                height: 5px;
                top: 45px;
                left: 130.475px;
            }

            #BOX289 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #GROUP287 {
                width: 409px;
                height: 50px;
                top: 10px;
                left: 5.5px;
            }

            #SHAPE309 {
                width: 23.7993px;
                height: 22.5469px;
                top: 0px;
                left: 0px;
            }

            #SHAPE309 svg:last-child {
                fill: rgba(255, 255, 255, 1);
            }

            #HEADLINE310 {
                width: 315px;
                top: 3px;
                left: 35px;
            }

            #HEADLINE310 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP308 {
                width: 350px;
                height: 41px;
                top: 132px;
                left: 26.317px;
            }

            #BUTTON_TEXT312 {
                width: 198px;
                top: 10.2724px;
                left: 0px;
            }

            #BUTTON_TEXT312 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON312 {
                width: 232.315px;
                height: 39.9483px;
                top: 154.362px;
                left: 65.7082px;
            }

            #BUTTON312 > .ladi-button > .ladi-button-background {
                background-color: rgb(73 221 255);
            }

            #FORM_ITEM315 {
                width: 363.732px;
                height: 39.9483px;
                top: 0px;
                left: 0px;
            }

            #FORM_ITEM317 {
                width: 363.732px;
                height: 39.9483px;
                top: 51px;
                left: 0px;
            }

            #FORM_ITEM318 {
                width: 363.732px;
                height: 40px;
                top: 101.5px;
                left: 0px;
            }

            #FORM311 {
                width: 363.732px;
                height: 194.31px;
                top: 200.641px;
                left: 27.951px;
            }

            #FORM311 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM311 .ladi-form-item .ladi-form-control::placeholder, #FORM311 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM311 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20%23000%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM311 .ladi-form-item-container, #FORM311 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM311 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #FORM311 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
                border-radius: 4px
            }

            #FORM312 {
                width: 363.732px;
                height: 185px;
                bottom: 115px;
                left: 50px;
            }

            #FORM312 > .ladi-form {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #FORM312 .ladi-form-item .ladi-form-control::placeholder, #FORM312 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: #000;
            }

            #FORM312 .ladi-form-item-container, #FORM312 .ladi-form-label-container .ladi-form-label-item {
                border-style: solid;
                border-color: rgb(180, 180, 180);
                border-width: 1px;
                border-radius: 5px;
            }

            #FORM312 .ladi-form-item-container .ladi-form-quantity button {
                background-color: rgb(180, 180, 180);
            }

            #FORM312 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
                border-radius: 4px
            }

            #CAROUSEL326 {
                width: 420px;
                height: 250.349px;
                top: 715px;
                left: 0px;
            }

            #CAROUSEL326 .ladi-carousel .ladi-carousel-arrow {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M7.00015%200.585938L18.4144%2012.0002L7.00015%2023.4144L5.58594%2022.0002L15.5859%2012.0002L5.58594%202.00015L7.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #IMAGE327 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 6px;
            }

            #IMAGE327 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car1.jpeg");
            }

            #IMAGE328 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 257px;
            }

            #IMAGE328 > .ladi-image > .ladi-image-background {
                width: 364.39px;
                height: 238px;
                top: 0px;
                left: -59.024px;
                background-image: url("/web/images/s700x550/5eba1da36b12637b2bd3f2e5/gia-xe-toyota-innova-2020-oto-co-20220802064747.png");
            }

            #IMAGE329 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 506px;
            }

            #IMAGE329 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car2.jpeg");
            }

            #IMAGE330 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 756px;
            }

            #IMAGE330 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/car3.jpeg");
            }

            #IMAGE331 {
                width: 238px;
                height: 238px;
                top: 6px;
                left: 1005px;
            }

            #IMAGE331 > .ladi-image > .ladi-image-background {
                width: 238px;
                height: 238px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s550x550/5eba1da36b12637b2bd3f2e5/xe-san-bay-airport2-653x440-2022-20220802064748.png");
            }

            #IMAGE332 {
                width: 200px;
                height: 112.474px;
                top: 70px;
                left: 110px;
            }

            #IMAGE332 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 112.474px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s550x450/5eba1da36b12637b2bd3f2e5/123-20220413161251-20220802074926.png");
            }

            #IMAGE333 {
                width: 200px;
                height: 112.474px;
                top: 192.474px;
                left: 110px;
            }

            #IMAGE333 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 112.474px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s550x450/5eba1da36b12637b2bd3f2e5/maxresdefault-20220413161251-20220802074926.png");
            }

            #HEADLINE334 {
                width: 112px;
                top: 12.5px;
                right: 15px;
            }

            #HEADLINE334 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }
            #hot-line-2 {
                width: 112px;
                top: 43.5px;
                right: 15px;
            }

            #hot-line-2 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            .logo {
                height: 85px;
                width: 85px;
                background-image: url("/web/logo.png");
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-position: center center;
                background-repeat: repeat;
                margin-left: 1px;
            }

            #HEADLINE335 {
                width: 171px;
                top: 10.5px;
                left: 249px;
            }

            #HEADLINE335 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 17px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE344 {
                width: 171px;
                top: 3.25275px;
                left: 47.7183px;
            }

            #HEADLINE344 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 28px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE344.ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #IMAGE343 {
                width: 47.7183px;
                height: 51.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE343 > .ladi-image > .ladi-image-background {
                width: 47.7183px;
                height: 51.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s350x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE343.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BOX342 {
                width: 180.377px;
                height: 38.046px;
                top: 6.72975px;
                left: 38.0593px;
            }

            #BOX342 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }

            #GROUP341 {
                width: 218.718px;
                height: 51.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 65px;
                position: fixed;
                z-index: 90000050;
                margin-left: calc((100% - 420px) / 2);
            }

            #GROUP305 {
                width: 409px;
                height: 50px;
                top: 25px;
                left: 5.5px;
            }

            #BOX307 {
                width: 148.049px;
                height: 5px;
                top: 45px;
                left: 130.475px;
            }

            #BOX307 > .ladi-box {
                background-color: rgb(73 221 255);
            }

            #HEADLINE306 {
                width: 409px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE306 > .ladi-headline {
                font-family: "Crimson Pro", serif;
                color: rgb(73 221 255);
                font-size: 25px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #IMAGE236 {
                width: 344px;
                height: 102.381px;
                top: 1318.5px;
                left: 31.951px;
            }

            #IMAGE236 > .ladi-image > .ladi-image-background {
                width: 344px;
                height: 102.381px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/5eba1da36b12637b2bd3f2e5/hotline-20211104103042.gif");
            }

            #GROUP175 {
                width: 396px;
                height: 271.248px;
                top: 681.877px;
                left: 12px;
            }

            #PARAGRAPH184 {
                width: 396px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH184 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE183 {
                width: 138px;
                top: 136.719px;
                left: 129.25px;
            }

            #HEADLINE183 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP177 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 152.886px;
            }

            #SHAPE182 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE182 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE181 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE181 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE180 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE180 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE179 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE179 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE178 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE178 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE176 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 134.65px;
            }

            #IMAGE176 > .ladi-image > .ladi-image-background {
                width: 162.406px;
                height: 243.562px;
                top: -116px;
                left: -30px;
                background-image: url("/web/customers/1.jpeg");
            }

            #IMAGE176 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP165 {
                width: 393px;
                height: 271.248px;
                top: 1006.12px;
                left: 13.5px;
            }

            #PARAGRAPH174 {
                width: 393px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH174 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE173 {
                width: 181px;
                top: 137.719px;
                left: 106px;
            }

            #HEADLINE173 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP167 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 153.886px;
            }

            #SHAPE172 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE172 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE171 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE171 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE170 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE170 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE169 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE169 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE168 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE168 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE166 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 135.65px;
            }

            #IMAGE166 > .ladi-image > .ladi-image-background {
                width: 124.7px;
                height: 124.7px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s450x450/5eba1da36b12637b2bd3f2e5/8206116b260ba5f42174b7c24286798d-20211104101342.jpg");
            }

            #IMAGE166 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP155 {
                width: 393px;
                height: 271.248px;
                top: 416.248px;
                left: 15px;
            }

            #PARAGRAPH164 {
                width: 393px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH164 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE163 {
                width: 138px;
                top: 136.719px;
                left: 128.25px;
            }

            #HEADLINE163 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP157 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 151.886px;
            }

            #SHAPE162 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE162 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE161 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE161 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE160 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE160 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE159 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE159 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE158 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE158 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE156 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 133.65px;
            }

            #IMAGE156 > .ladi-image > .ladi-image-background {
                width: 164.889px;
                height: 137.677px;
                top: 0px;
                left: -11.1644px;
                background-image: url("/web/images/s500x450/5eba1da36b12637b2bd3f2e5/chi-lan--20211111025719.jpg");
            }

            #IMAGE156 > .ladi-image {
                border-radius: 123px;
            }

            #GROUP154 {
                width: 398px;
                height: 296.248px;
                top: 125px;
                left: 11px;
            }

            #PARAGRAPH153 {
                width: 398px;
                top: 196.248px;
                left: 0px;
            }

            #PARAGRAPH153 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 16px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE151 {
                width: 138px;
                top: 136.719px;
                left: 131.25px;
            }

            #HEADLINE151 > .ladi-headline {
                font-family: "GoogleSans-Medium.ttf";
                color: rgb(67, 160, 248);
                font-size: 16px;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.4;
            }

            #GROUP145 {
                width: 88.2284px;
                height: 15.1578px;
                top: 166.669px;
                left: 154.886px;
            }

            #SHAPE150 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 74.926px;
            }

            #SHAPE150 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE149 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 56.1945px;
            }

            #SHAPE149 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE148 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 37.463px;
            }

            #SHAPE148 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE147 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 18.7315px;
            }

            #SHAPE147 svg:last-child {
                fill: rgb(244 202 74);
            }

            #SHAPE146 {
                width: 13.3024px;
                height: 15.1578px;
                top: 0px;
                left: 0px;
            }

            #SHAPE146 svg:last-child {
                fill: rgb(244 202 74);
            }

            #IMAGE142 {
                width: 124.7px;
                height: 123.719px;
                top: 0px;
                left: 136.65px;
            }

            #IMAGE142 > .ladi-image > .ladi-image-background {
                width: 161.166px;
                height: 130.166px;
                top: 0px;
                left: -35px;
                background-image: url("/web/customers/2.jpeg");
            }

            #IMAGE142 > .ladi-image {
                border-radius: 123px;
            }

            #SECTION97 {
                height: 1465.37px;
            }

            #SECTION97 .ladi-section-arrow-down {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%205.58594L12.0002%2015.5859L22.0002%205.58594L23.4144%207.00015L12.0002%2018.4144L0.585938%207.00015L2.00015%205.58594Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E");
            }

            #BOX346 {
                width: 143.102px;
                height: 25.0631px;
                top: 13.7208px;
                left: 35.7399px;
            }

            #BOX346 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }

            #BOX346-2 {
                width: 143.102px;
                height: 25.0631px;
                top: 13.7208px;
                left: 35.7399px;
            }

            #BOX346-2 > .ladi-box {
                background-color: rgb(203, 155, 86);
                border-top-right-radius: 14px;
                border-bottom-right-radius: 14px;
            }

            #IMAGE347 {
                width: 48.6446px;
                height: 52.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE347 > .ladi-image > .ladi-image-background {
                width: 48.6446px;
                height: 52.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s350x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE347.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #IMAGE347-2 {
                width: 48.6446px;
                height: 52.5054px;
                top: 0px;
                left: 0px;
            }

            #IMAGE347-2 > .ladi-image > .ladi-image-background {
                width: 48.6446px;
                height: 52.5054px;
                top: 0px;
                left: 0px;
                background-image: url("/web/images/s350x400/5eba1da36b12637b2bd3f2e5/ic1-20210910065644.png");
            }

            #IMAGE347-2 .ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #HEADLINE348 {
                width: 124px;
                top: 9.99151px;
                left: 51.4322px;
            }

            #HEADLINE348 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 20px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE348.ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }
            #HEADLINE348-2 {
                width: 124px;
                top: 9.99151px;
                left: 51.4322px;
            }

            #HEADLINE348-2 > .ladi-headline {
                font-family: "Tinos", serif;
                color: rgb(255, 255, 255);
                font-size: 20px;
                font-weight: bold;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE348-2 .ladi-animation > .ladi-headline {
                animation-name: fadeInRight;
                -webkit-animation-name: fadeInRight;
                animation-delay: 0.2s;
                -webkit-animation-delay: 0.2s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #GROUP345 {
                width: 178.842px;
                height: 52.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 13px;
                position: fixed;
                z-index: 90000050;
                margin-left: calc((100% - 420px) / 2);
            }
            #GROUP345-2 {
                width: 178.842px;
                height: 52.5054px;
                top: auto;
                left: 16px;
                right: auto;
                bottom: 70px;
                position: fixed;
                z-index: 90000050;
                margin-left: calc((100% - 420px) / 2);
            }

            #BOX367 {
                width: 54.9998px;
                height: 54.9998px;
                top: auto;
                left: 15px;
                right: auto;
                bottom: 120px;
                position: fixed;
                z-index: 90000050;
                margin-left: calc((100% - 420px) / 2);
            }

            #BOX367 > .ladi-box {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("/web/images/s400x400/5c7362c6c417ab07e5196b05/6aecd4d3a513e32a11391a56d260574b-1-20200424042407-20200706022304.png");
                background-position: center top;
                background-repeat: repeat;
                border-radius: 0px;
            }

            #BOX367.ladi-animation > .ladi-box {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }
        }

        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        #location {
            margin: 0 auto;
            font-size: 22px;
            font-weight: 900;
            color: rgb(243 129 71);
        }
    </style>
    <style id="style_lazyload"
           type="text/css">.ladi-section-background, .ladi-image-background, .ladi-button-background, .ladi-headline, .ladi-video-background, .ladi-countdown-background, .ladi-box, .ladi-frame-background, .ladi-tabs-background, .ladi-survey-option-background, .ladi-survey-option-image, .ladi-banner, .ladi-form-item-background, .ladi-gallery-view-item, .ladi-gallery-control-item, .ladi-spin-lucky-screen, .ladi-spin-lucky-start, .ladi-form-label-container .ladi-form-label-item.image, .ladi-list-paragraph ul li:before {
            background-image: none !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/web/css/modal-loading.css"/>
    <link rel="stylesheet" type="text/css" href="/web/css/modal-loading-animate.css"/>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4W4M8BK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="ladi-wraper">
    <div class="bnt1"></div>
    <div id="SECTION1" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container" style="text-align: center">
            <div class="div-hot-line">
                <div id="GROUP4" class='ladi-element'>
                    <div class='ladi-group'>
                        <div id="HEADLINE2" class='ladi-element'>
                            <h3 class='ladi-headline'>Hotline:<br></h3>
                        </div>
                        <div id="SHAPE3" class='ladi-element'>
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100"
                                     xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class=""
                                     fill="rgba(255, 255, 255, 1)">
                              <path
                                  d="M78.014,20.385c8.463,8.75,12.51,18.127,12.84,29.081c0.076,2.519-1.453,4.183-3.876,4.312  c-2.557,0.136-4.293-1.441-4.356-4.012c-0.134-5.394-1.357-10.521-4.033-15.211C72.491,23.871,63.191,18.302,50.95,17.603  c-1.358-0.077-2.631-0.218-3.586-1.305c-1.223-1.391-1.33-2.991-0.672-4.62c0.664-1.642,2.01-2.382,3.759-2.352  c7.969,0.135,15.321,2.353,21.955,6.761C74.697,17.61,76.787,19.437,78.014,20.385z M50.11,24.674  c-0.732-0.01-1.53,0.134-2.189,0.44c-1.704,0.79-2.505,2.791-2.048,4.786c0.402,1.758,1.954,2.972,3.906,2.996  c4.562,0.056,8.597,1.499,11.951,4.624c3.688,3.434,5.41,7.741,5.588,12.751c0.032,0.891,0.367,1.904,0.891,2.618  c1.094,1.49,3.037,1.864,4.821,1.184c1.577-0.601,2.506-2.014,2.492-3.886c-0.051-6.981-2.592-12.943-7.5-18.08  C63.098,27.364,57.118,24.773,50.11,24.674z M73.486,87.206c1.689-1.888,3.575-3.599,5.361-5.401  c2.643-2.667,2.664-5.902,0.036-8.55c-3.134-3.157-6.28-6.302-9.44-9.433c-2.586-2.562-5.819-2.556-8.393-0.005  c-1.966,1.948-3.936,3.893-5.86,5.882c-0.133,0.137-0.261,0.247-0.389,0.328l-1.346,1.346c-0.375,0.239-0.748,0.236-1.236-0.029  c-1.27-0.689-2.619-1.246-3.839-2.012c-5.695-3.575-10.471-8.183-14.694-13.374c-2.101-2.582-3.968-5.329-5.259-8.431  c-0.215-0.517-0.221-0.888,0.067-1.281l1.346-1.346c0.064-0.087,0.137-0.175,0.231-0.265c0.59-0.569,1.175-1.143,1.757-1.72  c1.361-1.348,2.706-2.711,4.057-4.069c2.69-2.703,2.684-5.88-0.015-8.604c-1.531-1.544-3.074-3.077-4.612-4.614  c-1.585-1.584-3.157-3.181-4.756-4.75c-2.59-2.543-5.824-2.548-8.408-0.007c-1.973,1.941-3.882,3.948-5.886,5.856  c-1.866,1.777-2.817,3.931-3.007,6.463c-0.307,4.104,0.699,7.983,2.106,11.77c2.909,7.832,7.333,14.766,12.686,21.137  c7.239,8.617,15.894,15.436,26.017,20.355c4.554,2.213,9.283,3.915,14.409,4.196C67.944,90.844,71.028,89.954,73.486,87.206z"></path>
                           </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="tel:0565771771" id="HEADLINE334" class='ladi-element'>
                    <h3 class='ladi-headline'>0565.771.771 </h3>
                </a>
            </div>

            <div class="logo">
            </div>

            <div class="div-hot-line">
                <div id="GROUP4-2" class='ladi-element'>
                    <div class='ladi-group-2'>
                        <div id="HEADLINE2-2" class='ladi-element'>
                            <h3 class='ladi-headline'>Hotline:<br></h3>
                        </div>
                        <div id="SHAPE3-2" class='ladi-element'>
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100"
                                     xml:space="preserve" preserveAspectRatio="none" width="100%" height="100%" class=""
                                     fill="rgba(255, 255, 255, 1)">
                              <path
                                  d="M78.014,20.385c8.463,8.75,12.51,18.127,12.84,29.081c0.076,2.519-1.453,4.183-3.876,4.312  c-2.557,0.136-4.293-1.441-4.356-4.012c-0.134-5.394-1.357-10.521-4.033-15.211C72.491,23.871,63.191,18.302,50.95,17.603  c-1.358-0.077-2.631-0.218-3.586-1.305c-1.223-1.391-1.33-2.991-0.672-4.62c0.664-1.642,2.01-2.382,3.759-2.352  c7.969,0.135,15.321,2.353,21.955,6.761C74.697,17.61,76.787,19.437,78.014,20.385z M50.11,24.674  c-0.732-0.01-1.53,0.134-2.189,0.44c-1.704,0.79-2.505,2.791-2.048,4.786c0.402,1.758,1.954,2.972,3.906,2.996  c4.562,0.056,8.597,1.499,11.951,4.624c3.688,3.434,5.41,7.741,5.588,12.751c0.032,0.891,0.367,1.904,0.891,2.618  c1.094,1.49,3.037,1.864,4.821,1.184c1.577-0.601,2.506-2.014,2.492-3.886c-0.051-6.981-2.592-12.943-7.5-18.08  C63.098,27.364,57.118,24.773,50.11,24.674z M73.486,87.206c1.689-1.888,3.575-3.599,5.361-5.401  c2.643-2.667,2.664-5.902,0.036-8.55c-3.134-3.157-6.28-6.302-9.44-9.433c-2.586-2.562-5.819-2.556-8.393-0.005  c-1.966,1.948-3.936,3.893-5.86,5.882c-0.133,0.137-0.261,0.247-0.389,0.328l-1.346,1.346c-0.375,0.239-0.748,0.236-1.236-0.029  c-1.27-0.689-2.619-1.246-3.839-2.012c-5.695-3.575-10.471-8.183-14.694-13.374c-2.101-2.582-3.968-5.329-5.259-8.431  c-0.215-0.517-0.221-0.888,0.067-1.281l1.346-1.346c0.064-0.087,0.137-0.175,0.231-0.265c0.59-0.569,1.175-1.143,1.757-1.72  c1.361-1.348,2.706-2.711,4.057-4.069c2.69-2.703,2.684-5.88-0.015-8.604c-1.531-1.544-3.074-3.077-4.612-4.614  c-1.585-1.584-3.157-3.181-4.756-4.75c-2.59-2.543-5.824-2.548-8.408-0.007c-1.973,1.941-3.882,3.948-5.886,5.856  c-1.866,1.777-2.817,3.931-3.007,6.463c-0.307,4.104,0.699,7.983,2.106,11.77c2.909,7.832,7.333,14.766,12.686,21.137  c7.239,8.617,15.894,15.436,26.017,20.355c4.554,2.213,9.283,3.915,14.409,4.196C67.944,90.844,71.028,89.954,73.486,87.206z"></path>
                           </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="tel:0335593959" id="hot-line-2" class='ladi-element'>
                    <h3 class='ladi-headline'>033.559.3959</h3>
                </a>
            </div>
        </div>
    </div>
    <div id="SECTION5" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <div data-action="true" id="HEADLINE6" class='ladi-element'>
                <h3 class='ladi-headline'>Trang chủ<br></h3>
            </div>
            <div data-action="true" id="HEADLINE7" class='ladi-element'>
                <h3 class='ladi-headline'>Đặt xe nhanh<br></h3>
            </div>
            <div data-action="true" id="HEADLINE8" class='ladi-element'>
                <h3 class='ladi-headline'>Đặt xe đi tour<br></h3>
            </div>
            <div data-action="true" id="HEADLINE9" class='ladi-element'>
                <h3 class='ladi-headline'>Đặt xe Công Tác<br></h3>
            </div>
            <div data-action="true" id="HEADLINE10" class='ladi-element'>
                <h3 class='ladi-headline'>Báo giá<br></h3>
            </div>
            <div data-action="true" id="HEADLINE260" class='ladi-element'>
                <h3 class='ladi-headline'>ĐỊA ĐIỂM DU LỊCH</h3>
            </div>
        </div>
    </div>
    <div id="SECTION11" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-overlay"></div>
        <div class="ladi-container">
            <div id="GROUP24" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="BOX12" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                    <div id="BOX13" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                    <div id="HEADLINE14" class='ladi-element'>
                        <h3 class='ladi-headline'>ĐẶT XE NHANH GIÁ TỐT</h3>
                    </div>
                    <div id="FORM15" class='ladi-element'>
                        <form method="post" class='ladi-form' method="post"
                              action="{{ url('/message/store') }}">
                            @csrf
                            <div id="BUTTON16-16" class='ladi-element'>
                                <div class='ladi-button-01'>
                                    <div class="ladi-button-background"></div>
                                </div>
                            </div>
                            <div id="FORM_ITEM17" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <input autocomplete="off" tabindex="1" name="name"
                                               required class="ladi-form-control" type="text"
                                               placeholder="💁 Nhập họ và tên" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div id="FORM_ITEM19" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <input autocomplete="off" tabindex="3" name="phone"
                                               required class="ladi-form-control" type="tel"
                                               placeholder="📞 Nhập số điện thoại"
                                               pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}"
                                               value=""/>
                                    </div>
                                </div>
                            </div>
                            <div id="FORM_ITEM20" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <textarea autocomplete="off" tabindex="4" name="note"
                                                  class="ladi-form-control"
                                                  placeholder="Để lại lời nhắn cho chúng tôi">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div id="FORM_ITEM21" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'><input autocomplete="off" tabindex="4" name="address"
                                                                       required class="ladi-form-control" type="text"
                                                                       placeholder="🚀 Nhập điểm đón" value=""/></div>
                                </div>
                            </div>
                            <div id="FORM_ITEM22" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'><input autocomplete="off" tabindex="4" name="to_address"
                                                                       required class="ladi-form-control" type="text"
                                                                       placeholder="🎯 Nhập điểm đến" value=""/></div>
                                </div>
                            </div>
                            <div id="FORM_ITEM23" class='ladi-element'>
                                <div class="ladi-form-item-container" style="max-width: 50%; left: 0px">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <input autocomplete="off" name="email"
                                               class="ladi-form-control" type="email"
                                               placeholder="📧 Nhập email" value=""/>
                                    </div>
                                </div>
                                <div class="ladi-form-item-container" style="max-width: 45%; right: 0px">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <input autocomplete="off" name="date_time"
                                               required class="ladi-form-control" type="text"
                                               placeholder="⏳ Nhập Thời Gian" value=""/>
                                    </div>
                                </div>
                            </div>
                            <div id="FORM_ITEM24" class='ladi-element'>
                                <div class="ladi-form-item-container">
                                    <div class="ladi-form-item-background"></div>
                                    <div class='ladi-form-item'>
                                        <select tabindex="6" name="type_car"
                                                class="ladi-form-control ladi-form-control-select"
                                                data-selected="🚘 Xe 4 chỗ">
                                            <option value="4" selected="selected">🚘 Xe 4 chỗ</option>
                                            <option value="7">🚘 Xe 7 chỗ</option>
                                            <option value="0">🚘 Xe bán tải</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="button-submit"
                                 style="text-align: center;height: 40px;top: 374.224px; left: 0px; right: 0px; position: relative">
                                <button type="submit"
                                        style="background-color: rgb(73 221 255); text-align: center; padding: 5px; border-radius: 5px"
                                        id="submit_booking" class='ladi-button-headline-01'>
                                        <span
                                            style="color: rgb(255, 255, 255);font-size: 14px;font-weight: bold;text-align: center;line-height: 1.6;">
                                            GỬI YÊU CẦU ĐẶT XE - BÁO GIÁ
                                        </span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="LIST_PARAGRAPH27" class='ladi-element'>
                <div class='ladi-list-paragraph'>
                    <ul>
                        <li>Thời gian từ : 4h &lt;—&gt; 22h&nbsp;</li>
                        <li>Vĩnh Phúc &lt;-&gt; Hà Nội&nbsp;</li>
                        <li>Nhận bao xe giá rẻ (ship đồ 100k)</li>
                        <li>Đón và trả khách tận nơi&nbsp;</li>
                    </ul>
                </div>
            </div>
            <div id="IMAGE259" class='ladi-element'>
                <div class='ladi-image'>
                    <div class="ladi-image-background"></div>
                </div>
            </div>
            <div id="HEADLINE261" class='ladi-element'>
                <h3 class='nha-xe'>XE GHÉP BẢO BẢO</h3>
                <h5 class='ladi-headline'>XE GHÉP CHẠY HÀNG NGÀY</h5>
            </div>
        </div>
    </div>
    <div id="SECTION28" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <div id="GROUP31" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="HEADLINE29" class='ladi-element'>
                        <h3 class='ladi-headline'>GHÉP XE VĨNH PHÚC - HÀ NỘI MANG LẠI CHO KHÁCH HÀNG<br></h3>
                    </div>
                    <div id="BOX30" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                </div>
            </div>
            <div id="GROUP35" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE32" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE33" class='ladi-element'>
                        <h3 class='ladi-headline'>tiết kiệm</h3>
                    </div>
                    <div id="HEADLINE34" class='ladi-element'>
                        <h3 class='ladi-headline'>Cam kết giá rẻ nhất và hỗ trợ<br>khi khách liên hệ lại<br></h3>
                    </div>
                </div>
            </div>
            <div id="GROUP36" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE37" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE38" class='ladi-element'>
                        <h3 class='ladi-headline'>trọn gói</h3>
                    </div>
                    <div id="HEADLINE39" class='ladi-element'>
                        <h3 class='ladi-headline'>Khách biết giá trước khi đi và có dịch vụ chờ<br></h3>
                    </div>
                </div>
            </div>
            <div id="GROUP40" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE41" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE42" class='ladi-element'>
                        <h3 class='ladi-headline'>CHẤT LƯỢNG XE</h3>
                    </div>
                    <div id="HEADLINE43" class='ladi-element'>
                        <h3 class='ladi-headline'>Xe đời mới - Sang trọng hơn<br></h3>
                    </div>
                </div>
            </div>
            <div id="GROUP44" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE45" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE46" class='ladi-element'>
                        <h3 class='ladi-headline'>DI CHUYỂN AN TOÀN</h3>
                    </div>
                    <div id="HEADLINE47" class='ladi-element'>
                        <h3 class='ladi-headline'>Tài xế giàu kinh nghiệm, chạy Tour được</h3>
                    </div>
                </div>
            </div>
            <div id="HEADLINE48" class='ladi-element'>
                <h3 class='ladi-headline'>“Nhà Xe Bảo Bảo, với đội ngũ lái xe chuyên nghiệp, lịch sự và có nhiều năm kinh nghiệm chạy xe đường dài. Chúng tôi cung cấp dịch vụ xe ghép - xe tiện chuyến - bao xe đi công tác - bao xe đi du lịch, các tuyến: </h3>
                <div id="location">
                    <ol>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Vĩnh Yên</li>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Vĩnh Tường</li>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Lập Thạch</li>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Yên Lạc</li>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Bình Xuyên</li>
                        <li>📍 Hà Nội <span style="font-size: 18px; margin-bottom: 15px; font-weight: bold"><-></span> Tam Dương</li>
                        <li style="font-size: 16px">📌 Nhà xe đưa đón tận nơi các khực lân cận quanh nội thành.</li>
                    </ol>
                </div>
                <h3 class='ladi-headline' style="margin-bottom: 15px">Nhận Ship hàng chỉ từ 100k - Liên hệ ngay để có thể nhận được ưu đãi về giá 📞0565.771.771.”</h3>
                <h3 class='ladi-headline' style="margin-bottom: 15px; color: orangered">🚗 Đặc Biệt Nhà Xe luôn lưu tâm tới những khác hàng cần đi khám bệnh hoặc đi công tác, Nhà xe Bảo Bảo luôn giữ: LỊCH SỰ - ĐÚNG LỊCH - ĐÚNG GIỜ </h3>

                <h3 class='ladi-headline'>“Chúng tôi luôn xem trọng dịch vụ mà chúng tôi mang lại là đặt xe nhanh chóng
                    – giá thành cạnh tranh luôn hấp dẫn so với thị trường xe tour, xe đường dài và luôn đặt sự an toàn
                    của quý khách là ưu tiên hàng đầu.”<br></h3>
            </div>
            <div id="LINE49" class='ladi-element'>
                <div class='ladi-line'>
                    <div class="ladi-line-container"></div>
                </div>
            </div>
            <div id="LINE50" class='ladi-element'>
                <div class='ladi-line'>
                    <div class="ladi-line-container"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="SECTION55" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <a href="tel:0565771771" id="IMAGE235" class='ladi-element'>
                <div class='ladi-image'>
                    <div class="ladi-image-background"></div>
                </div>
            </a>
            <div id="CAROUSEL326" class='ladi-element'>
                <div class="carousel"></div>
            </div>
        </div>
    </div>
    <div id="SECTION51" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <div id="GROUP287" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="HEADLINE288" class='ladi-element'>
                        <h3 class='ladi-headline'>địa điểm phổ biến</h3>
                    </div>
                    <div id="BOX289" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                </div>
            </div>
            <div id="IMAGE332" class='ladi-element'>
                <div class='ladi-image'>
                    <div class="ladi-image-background"></div>
                </div>
            </div>
            <div id="IMAGE333" class='ladi-element'>
                <div class='ladi-image'>
                    <div class="ladi-image-background"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="SECTION97" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <div id="GROUP154" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE142" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="GROUP145" class='ladi-element'>
                        <div class='ladi-group'>
                            <div id="SHAPE146" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE147" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE148" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE149" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE150" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1137 1004l306-297-422-62-189-382-189 382-422 62 306 297-73 421 378-199 377 199zm527-357q0 22-26 48l-363 354 86 500q1 7 1 20 0 50-41 50-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE151" class='ladi-element'>
                        <h4 class='ladi-headline'>CHỊ MINH</h4>
                    </div>
                    <div id="PARAGRAPH153" class='ladi-element'>
                        <p class='ladi-paragraph'>"Đi đường dài mong muốn nhất là xe thoải mái, sạch sẽ và không bị ồn
                            ào. Rất may vì đã gặp được <span style="color: rgb(67, 160, 248);">Xe Ghép Bảo Bảo</span>&nbsp;tận
                            tình và thân thiện với khách."</p>
                    </div>
                </div>
            </div>
            <div id="GROUP155" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE156" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="GROUP157" class='ladi-element'>
                        <div class='ladi-group'>
                            <div id="SHAPE158" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE159" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE160" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE161" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE162" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1137 1004l306-297-422-62-189-382-189 382-422 62 306 297-73 421 378-199 377 199zm527-357q0 22-26 48l-363 354 86 500q1 7 1 20 0 50-41 50-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE163" class='ladi-element'>
                        <h4 class='ladi-headline'>CHỊ PHƯƠNG</h4>
                    </div>
                    <div id="PARAGRAPH164" class='ladi-element'>
                        <p class='ladi-paragraph'>"Ở đây không chỉ có dịch vụ tốt , giá lại cạnh tranh nên tôi rất hài
                            lòng khi sử dụng dịch vụ của <span
                                style="color: rgb(67, 160, 248);">Xe Ghép Bảo Bảo</span>."<br></p>
                    </div>
                </div>
            </div>
            <div id="GROUP165" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE166" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="GROUP167" class='ladi-element'>
                        <div class='ladi-group'>
                            <div id="SHAPE168" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE169" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE170" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE171" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE172" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1137 1004l306-297-422-62-189-382-189 382-422 62 306 297-73 421 378-199 377 199zm527-357q0 22-26 48l-363 354 86 500q1 7 1 20 0 50-41 50-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE173" class='ladi-element'>
                        <h4 class='ladi-headline'>GIA ĐÌNH ANH PHÚC</h4>
                    </div>
                    <div id="PARAGRAPH174" class='ladi-element'>
                        <p class='ladi-paragraph'>"Tài xế lái xe cẩn thận, chi phí chuyến đi được báo trước khi di
                            chuyển nên khi du lịch gia đình tôi luôn tin tưởng <span style="color: rgb(67, 160, 248);">Xe Ghép Bảo Bảo</span>"
                        </p>
                    </div>
                </div>
            </div>
            <div id="GROUP175" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="IMAGE176" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="GROUP177" class='ladi-element'>
                        <div class='ladi-group'>
                            <div id="SHAPE178" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE179" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE180" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE181" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1664 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5T1321 1619q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div id="SHAPE182" class='ladi-element'>
                                <div class='ladi-shape'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                         preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                         fill="rgb(244 202 74)">
                                        <path
                                            d="M1137 1004l306-297-422-62-189-382-189 382-422 62 306 297-73 421 378-199 377 199zm527-357q0 22-26 48l-363 354 86 500q1 7 1 20 0 50-41 50-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5T301 1569q0-6 2-20l86-500L25 695Q0 668 0 647q0-37 56-46l502-73L783 73q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HEADLINE183" class='ladi-element'>
                        <h4 class='ladi-headline'>CHỊ THANH MAI</h4>
                    </div>
                    <div id="PARAGRAPH184" class='ladi-element'>
                        <p class='ladi-paragraph'>"Trong một lần đi gặp đối tác để kí hợp đồng tôi đã lựa chọn <span
                                style="color: rgb(67, 160, 248);">Xe Ghép Bảo Bảo</span>, tôi hài lòng vì sự minh bạch
                            về chi phí của họ nên khi di chuyển đi đến đâu tôi đều nghĩ đến <span
                                style="color: rgb(67, 160, 248);">Xe Ghép Bảo Bảo</span>&nbsp;đầu tiên"<br></p>
                    </div>
                </div>
            </div>
            <a href="tel:0565771771" id="IMAGE236" class='ladi-element'>
                <div class='ladi-image'>
                    <div class="ladi-image-background"></div>
                </div>
            </a>
            <div id="GROUP305" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="HEADLINE306" class='ladi-element'>
                        <h3 class='ladi-headline'>KHÁCH HÀNG NHẬN XÉT VỀ DỊCH VỤ<br></h3>
                    </div>
                    <div id="BOX307" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="SECTION120" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-overlay"></div>
        <div class="ladi-container">
            <div id="HEADLINE185" class='ladi-element'>
                <h3 class='ladi-headline'>LIÊN HỆ CHÚNG TÔI NGAY:<br></h3>
            </div>
            <div id="LINE219" class='ladi-element'>
                <div class='ladi-line'>
                    <div class="ladi-line-container"></div>
                </div>
            </div>
            <div id="HEADLINE220" class='ladi-element'>
                <h1 class='ladi-headline'>©2022 Allrights reserved Xe Ghép Bảo Bảo</h1>
            </div>
            <div id="GROUP226" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="SHAPE227" class='ladi-element'>
                        <div class='ladi-shape'>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,255,255,1)">
                                <path
                                    d="M6.62,10.79C8.06,13.62 10.38,15.94 13.21,17.38L15.41,15.18C15.69,14.9 16.08,14.82 16.43,14.93C17.55,15.3 18.75,15.5 20,15.5A1,1 0 0,1 21,16.5V20A1,1 0 0,1 20,21A17,17 0 0,1 3,4A1,1 0 0,1 4,3H7.5A1,1 0 0,1 8.5,4C8.5,5.25 8.7,6.45 9.07,7.57C9.18,7.92 9.1,8.31 8.82,8.59L6.62,10.79Z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE228" class='ladi-element'>
                        <p class='ladi-headline'>Hotline : 0565.771.771<br></p>
                        <p class='ladi-headline' style="margin-bottom: 10px">Hotline : 033.559.3959</p><br>
                    </div>
                </div>
            </div>
            <div id="GROUP232" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="SHAPE233" class='ladi-element'>
                        <div class='ladi-shape'>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,255,255,1)">
                                <path
                                    d="M16.36,14C16.44,13.34 16.5,12.68 16.5,12C16.5,11.32 16.44,10.66 16.36,10H19.74C19.9,10.64 20,11.31 20,12C20,12.69 19.9,13.36 19.74,14M14.59,19.56C15.19,18.45 15.65,17.25 15.97,16H18.92C17.96,17.65 16.43,18.93 14.59,19.56M14.34,14H9.66C9.56,13.34 9.5,12.68 9.5,12C9.5,11.32 9.56,10.65 9.66,10H14.34C14.43,10.65 14.5,11.32 14.5,12C14.5,12.68 14.43,13.34 14.34,14M12,19.96C11.17,18.76 10.5,17.43 10.09,16H13.91C13.5,17.43 12.83,18.76 12,19.96M8,8H5.08C6.03,6.34 7.57,5.06 9.4,4.44C8.8,5.55 8.35,6.75 8,8M5.08,16H8C8.35,17.25 8.8,18.45 9.4,19.56C7.57,18.93 6.03,17.65 5.08,16M4.26,14C4.1,13.36 4,12.69 4,12C4,11.31 4.1,10.64 4.26,10H7.64C7.56,10.66 7.5,11.32 7.5,12C7.5,12.68 7.56,13.34 7.64,14M12,4.03C12.83,5.23 13.5,6.57 13.91,8H10.09C10.5,6.57 11.17,5.23 12,4.03M18.92,8H15.97C15.65,6.75 15.19,5.55 14.59,4.44C16.43,5.07 17.96,6.34 18.92,8M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE234" class='ladi-element'>
                        <p class='ladi-headline'>Website: xeghephanoivinhphuc24h.net</p>
                    </div>
                </div>
            </div>
            <div id="GROUP308" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="SHAPE309" class='ladi-element'>
                        <div class='ladi-shape'>
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                                 preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" class=""
                                 fill="rgba(255,255,255,1)">
                                <path
                                    d="M1408 992v480q0 26-19 45t-45 19H960v-384H704v384H320q-26 0-45-19t-19-45V992q0-1 .5-3t.5-3l575-474 575 474q1 2 1 6zm223-69l-62 74q-8 9-21 11h-3q-13 0-21-7L832 424l-692 577q-12 8-24 7-13-2-21-11l-62-74q-8-10-7-23.5T37 878l719-599q32-26 76-26t76 26l244 204V288q0-14 9-23t23-9h192q14 0 23 9t9 23v408l219 182q10 8 11 21.5t-7 23.5z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="HEADLINE310" class='ladi-element'>
                        <p class='ladi-headline'>Địa chỉ : TP Vĩnh Yên , Vĩnh Phúc<br></p>
                    </div>
                </div>
            </div>
            <div id="FORM311" data-config-id="62eb3f0bb2dc570021b0ac3b" class='ladi-element'>
                <form autocomplete="off" method="post" class='ladi-form' action="{{ url('/message/store') }}">
                    @csrf
                    <div id="BUTTON312" class='ladi-element' style="padding: 15px; text-align: center">
                        <div class='ladi-button'>
                            <button type="submit" id="BUTTON_TEXT312"
                                    class='ladi-element '
                                    style="background-color: rgb(73 221 255); color: white; font-weight: bold; padding: 10px 15px 10px 15px; top: 0;">
                                ĐĂNG KÝ DỊCH VỤ
                            </button>
                        </div>
                    </div>
                    <div id="FORM_ITEM315" class='ladi-element'>
                        <div class="ladi-form-item-container">
                            <div class="ladi-form-item-background"></div>
                            <div class='ladi-form-item'>
                                <input autocomplete="off" tabindex="3" name="phone" required
                                       class="ladi-form-control" type="tel"
                                       placeholder="📞 Nhập số điện thoại"
                                       pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" value=""/>
                            </div>
                        </div>
                    </div>
                    <div id="FORM_ITEM317" class='ladi-element'>
                        <div class="ladi-form-item-container">
                            <div class="ladi-form-item-background"></div>
                            <div class='ladi-form-item'>
                                <input autocomplete="off" tabindex="4" name="address" required
                                       class="ladi-form-control" type="text"
                                       placeholder="🚀 Nhập điểm đón" value=""/></div>
                        </div>
                    </div>
                    <div id="FORM_ITEM318" class='ladi-element'>
                        <div class="ladi-form-item-container">
                            <div class="ladi-form-item-background"></div>
                            <div class='ladi-form-item'>
                                <input autocomplete="off" tabindex="5" name="diem_den" required
                                       class="ladi-form-control" type="text"
                                       placeholder="🎯 Nhập điểm đến" value=""/></div>
                        </div>
                    </div>
                </form>
            </div>

            <div id="FORM312" data-config-id="62eb3f0bb2dc570021b0ac3b" class='ladi-element'>
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FXeghephanoivinhphucchatluong&tabs=plugin&width=300&height=150&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1412776856331942" width="300" height="150" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
            </div>

            <a href="tel:0565771771" id="GROUP345" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="BOX346" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                    <div id="IMAGE347" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE348" class='ladi-element'>
                        <h3 class='ladi-headline'>0565.771.771</h3>
                    </div>
                </div>
            </a>
            <a href="tel:0335593959" id="GROUP345-2" class='ladi-element'>
                <div class='ladi-group'>
                    <div id="BOX346-2" class='ladi-element'>
                        <div class='ladi-box'></div>
                    </div>
                    <div id="IMAGE347-2" class='ladi-element'>
                        <div class='ladi-image'>
                            <div class="ladi-image-background"></div>
                        </div>
                    </div>
                    <div id="HEADLINE348-2" class='ladi-element'>
                        <h3 class='ladi-headline'>033.559.3959</h3>
                    </div>
                </div>
            </a>
            <a href="https://zalo.me/0565771771" target="_blank" id="BOX367" class='ladi-element'>
                <div class='ladi-box'></div>
            </a>
        </div>
    </div>
    <div id="SECTION_POPUP" class='ladi-section'>
        <div class='ladi-section-background'></div>
        <div class="ladi-container">
            <div id="POPUP245" class='ladi-element'>
                <div class='ladi-popup'>
                    <div class="ladi-popup-background"></div>
                    <div id="GROUP246" class='ladi-element'>
                        <div class='ladi-group'>
                            <div id="BOX247" class='ladi-element'>
                                <div class='ladi-box'></div>
                            </div>
                            <div id="BOX248" class='ladi-element'>
                                <div class='ladi-box'></div>
                            </div>
                            <div id="HEADLINE249" class='ladi-element'>
                                <h3 class='ladi-headline'>ĐẶT XE NHANH GIÁ TỐT</h3>
                            </div>
                            <div id="FORM250" data-config-id="62eb3f0bb2dc570021b0ac3b" class='ladi-element'>
                                <form autocomplete="off" method="post" class='ladi-form'>
                                    <div id="BUTTON251" class='ladi-element'>
                                        <div class='ladi-button'>
                                            <div class="ladi-button-background"></div>
                                            <div id="BUTTON_TEXT251" class='ladi-element ladi-button-headline-01'>
                                                <p class='ladi-headline'>GỬI YÊU CẦU ĐẶT XE - BÁO GIÁ</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM253" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'><input autocomplete="off" tabindex="1"
                                                                               name="name" required
                                                                               class="ladi-form-control" type="text"
                                                                               placeholder="💁 Nhập họ và tên"
                                                                               value=""/></div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM254" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'><input autocomplete="off" tabindex="3"
                                                                               name="phone" required
                                                                               class="ladi-form-control" type="tel"
                                                                               placeholder="📞 Nhập số điện thoại"
                                                                               pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}"
                                                                               value=""/></div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM255" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'><textarea autocomplete="off" tabindex="4"
                                                                                  name="message" required
                                                                                  class="ladi-form-control"
                                                                                  placeholder="Để lại lời nhắn cho chúng tôi"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM256" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'>
                                                <input autocomplete="off" tabindex="4"
                                                       name="address" required
                                                       class="ladi-form-control" type="text"
                                                       placeholder="🚀 Nhập điểm đón" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM257" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'>
                                                <input autocomplete="off" tabindex="5"
                                                       name="diem_den" class="ladi-form-control"
                                                       type="text"
                                                       placeholder="🎯 Nhập điểm đến" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM259" class='ladi-element'>
                                        <div class="ladi-form-item-container" style="max-width: 50%; left: 0px">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'>
                                                <input autocomplete="off" tabindex="5" name="email"
                                                       class="ladi-form-control" type="email"
                                                       placeholder="📧 Nhập email" value=""/>
                                            </div>
                                        </div>
                                        <div class="ladi-form-item-container" style="max-width: 45%; right: 0px">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'>
                                                <input autocomplete="off" tabindex="5" name="time"
                                                       required class="ladi-form-control" type="text"
                                                       placeholder="⏳ Nhập Thời Gian" value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="FORM_ITEM258" class='ladi-element'>
                                        <div class="ladi-form-item-container">
                                            <div class="ladi-form-item-background"></div>
                                            <div class='ladi-form-item'>
                                                <select tabindex="6" name="type_car"
                                                        class="ladi-form-control ladi-form-control-select"
                                                        data-selected="🚘 Xe 4 chỗ">
                                                    <option value="4" selected="selected">🚘 Xe 4 chỗ</option>
                                                    <option value="7">🚘 Xe 7 chỗ</option>
                                                    <option value="0">🚘 Xe bán tải</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="backdrop-popup" class="backdrop-popup"></div>
<div id="backdrop-dropbox" class="backdrop-dropbox"></div>
<div id="lightbox-screen" class="lightbox-screen"></div>
<script id="script_lazyload" type="text/javascript">(function () {
        var style_lazyload = document.getElementById('style_lazyload');
        var docEventScroll = window;
        var list_element_lazyload = document.querySelectorAll('.ladi-section-background, .ladi-image-background, .ladi-button-background, .ladi-headline, .ladi-video-background, .ladi-countdown-background, .ladi-box, .ladi-frame-background, .ladi-tabs-background, .ladi-survey-option-background, .ladi-survey-option-image, .ladi-banner, .ladi-form-item-background, .ladi-gallery-view-item, .ladi-gallery-control-item, .ladi-spin-lucky-screen, .ladi-spin-lucky-start, .ladi-form-label-container .ladi-form-label-item.image, .ladi-list-paragraph ul li');
        for (var i = 0; i < list_element_lazyload.length; i++) {
            var rect = list_element_lazyload[i].getBoundingClientRect();
            if (rect.x == "undefined" || rect.x == undefined || rect.y == "undefined" || rect.y == undefined) {
                rect.x = rect.left;
                rect.y = rect.top;
            }
            var offset_top = rect.y + window.scrollY;
            if (offset_top >= window.scrollY + window.innerHeight || window.scrollY >= offset_top + list_element_lazyload[i].offsetHeight) {
                list_element_lazyload[i].classList.add('ladi-lazyload');
            }
        }
        if (typeof style_lazyload != "undefined" && style_lazyload != undefined) {
            style_lazyload.parentElement.removeChild(style_lazyload);
        }
        var currentScrollY = window.scrollY;
        var stopLazyload = function (event) {
            if (event.type == "scroll" && window.scrollY == currentScrollY) {
                currentScrollY = -1;
                return;
            }
            docEventScroll.removeEventListener('scroll', stopLazyload);
            list_element_lazyload = document.getElementsByClassName('ladi-lazyload');
            while (list_element_lazyload.length > 0) {
                list_element_lazyload[0].classList.remove('ladi-lazyload');
            }
        };
        var scrollEventPassive = null;
        try {
            var opts = Object.defineProperty({}, 'passive', {
                get: function () {
                    scrollEventPassive = {passive: true};
                }
            });
            window.addEventListener('testPassive', null, opts);
            window.removeEventListener('testPassive', null, opts);
        } catch (e) {
        }
        docEventScroll.addEventListener('scroll', stopLazyload, scrollEventPassive);
    })();</script>

<script src="https://w.ladicdn.com/v2/source/html5shiv.min.js"></script>
<script src="https://w.ladicdn.com/v2/source/respond.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Crimson%20Pro:bold,regular|Tinos:bold,regular&amp;display=swap"
      rel="stylesheet" type="text/css">
<link href="/web/images/v2/source/ladipage.minb1b7.css" rel="stylesheet" type="text/css">
<script src="/web/images/v2/source/ladipage.vi.minb1b7.js" type="text/javascript"></script>
<script id="script_event_data" type="application/json">{
    "HEADLINE6": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "section",
                "action": "SECTION1",
                "action_type": "action"
            }
        ]
    },
    "HEADLINE7": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "popup",
                "action": "POPUP245",
                "action_type": "action"
            }
        ]
    },
    "HEADLINE8": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "popup",
                "action": "POPUP245",
                "action_type": "action"
            }
        ]
    },
    "HEADLINE9": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "popup",
                "action": "POPUP245",
                "action_type": "action"
            }
        ]
    },
    "HEADLINE10": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "popup",
                "action": "POPUP245",
                "action_type": "action"
            }
        ]
    },
    "FORM_ITEM17": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 1
    },
    "FORM_ITEM19": {
        "type": "form_item",
        "option.input_type": "tel",
        "option.input_tabindex": 3
    },
    "FORM_ITEM20": {
        "type": "form_item",
        "option.input_type": "textarea",
        "option.input_tabindex": 4
    },
    "FORM_ITEM21": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 4
    },
    "FORM_ITEM22": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 5
    },
    "FORM_ITEM23": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 5
    },
    "FORM_ITEM24": {
        "type": "form_item",
        "option.input_type": "select",
        "option.input_default_value": "🚘 Xe 4 chỗ",
        "option.input_tabindex": 6
    },
    "IMAGE235": {
        "type": "image",
        "option.data_event": [
            {
                "action_type": "action",
                "type": "phone",
                "action": "0934608365"
            }
        ]
    },
    "POPUP245": {
        "type": "popup",
        "desktop.option.popup_position": "default",
        "desktop.option.popup_backdrop": "background-color: rgba(0, 0, 0, 0.5);",
        "mobile.option.popup_position": "default",
        "mobile.option.popup_backdrop": "background-color: rgba(0, 0, 0, 0.5);"
    },
    "FORM_ITEM253": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 1
    },
    "FORM_ITEM254": {
        "type": "form_item",
        "option.input_type": "tel",
        "option.input_tabindex": 3
    },
    "FORM_ITEM255": {
        "type": "form_item",
        "option.input_type": "textarea",
        "option.input_tabindex": 4
    },
    "FORM_ITEM256": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 4
    },
    "FORM_ITEM257": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 5
    },
    "FORM_ITEM258": {
        "type": "form_item",
        "option.input_type": "select",
        "option.input_default_value": "🚘 Xe 4 chỗ",
        "option.input_tabindex": 6
    },
    "FORM250": {
        "type": "form",
        "option.form_send_ladipage": true,
        "option.thankyou_type": "default",
        "option.thankyou_value": "Cảm ơn bạn đã quan tâm!",
        "option.form_conversion_name": "CompleteRegistration",
        "option.form_captcha": true,
        "option.form_auto_complete": true
    },
    "IMAGE259": {
        "type": "image",
        "desktop.style.animation-name": "pulse",
        "desktop.style.animation-delay": "1s",
        "mobile.style.animation-name": "pulse",
        "mobile.style.animation-delay": "1s"
    },
    "HEADLINE260": {
        "type": "headline",
        "option.data_event": [
            {
                "type": "section",
                "action": "SECTION51",
                "action_type": "action"
            }
        ]
    },
    "FORM_ITEM315": {
        "type": "form_item",
        "option.input_type": "tel",
        "option.input_tabindex": 3
    },
    "FORM_ITEM317": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 4
    },
    "FORM_ITEM318": {
        "type": "form_item",
        "option.input_type": "text",
        "option.input_tabindex": 5
    },
    "FORM311": {
        "type": "form",
        "option.form_send_ladipage": true,
        "option.thankyou_type": "default",
        "option.thankyou_value": "Cảm ơn bạn đã quan tâm!",
        "option.form_conversion_name": "CompleteRegistration",
        "option.form_captcha": true,
        "option.form_auto_complete": true
    },
    "CAROUSEL326": {
        "type": "carousel",
        "desktop.option.carousel_setting.autoplay": true,
        "desktop.option.carousel_setting.autoplay_time": 5,
        "desktop.option.carousel_crop.width": "1250px",
        "desktop.option.carousel_crop.width_item": "250px",
        "mobile.option.carousel_setting.autoplay": true,
        "mobile.option.carousel_setting.autoplay_time": 5,
        "mobile.option.carousel_crop.width": "1250px",
        "mobile.option.carousel_crop.width_item": "250px"
    },
    "HEADLINE334": {
        "type": "headline",
        "option.data_event": [
            {
                "action_type": "action",
                "type": "phone",
                "action": "0934608365"
            }
        ]
    },
    "HEADLINE335": {
        "type": "headline",
        "option.data_event": [
            {
                "action_type": "action",
                "type": "phone",
                "action": "0856860860"
            }
        ]
    },
    "HEADLINE344": {
        "type": "headline",
        "desktop.style.animation-name": "fadeInRight",
        "desktop.style.animation-delay": "0.2s",
        "mobile.style.animation-name": "fadeInRight",
        "mobile.style.animation-delay": "0.2s"
    },
    "IMAGE343": {
        "type": "image",
        "desktop.style.animation-name": "pulse",
        "desktop.style.animation-delay": "0.2s",
        "mobile.style.animation-name": "pulse",
        "mobile.style.animation-delay": "0.2s"
    },
    "GROUP341": {
        "type": "group",
        "option.data_event": [
            {
                "type": "phone",
                "action": "0934608365",
                "action_type": "action"
            }
        ],
        "desktop.option.sticky": true,
        "desktop.option.sticky_position": "bottom_left",
        "desktop.option.sticky_position_top": "0px",
        "desktop.option.sticky_position_left": "16px",
        "desktop.option.sticky_position_bottom": "78px",
        "desktop.option.sticky_position_right": "0px",
        "mobile.option.sticky": true,
        "mobile.option.sticky_position": "bottom_left",
        "mobile.option.sticky_position_top": "0px",
        "mobile.option.sticky_position_left": "16px",
        "mobile.option.sticky_position_bottom": "65px",
        "mobile.option.sticky_position_right": "0px"
    },
    "IMAGE236": {
        "type": "image",
        "option.data_event": [
            {
                "type": "phone",
                "action": "0934608365",
                "action_type": "action"
            }
        ]
    },
    "IMAGE347": {
        "type": "image",
        "desktop.style.animation-name": "pulse",
        "desktop.style.animation-delay": "0.2s",
        "mobile.style.animation-name": "pulse",
        "mobile.style.animation-delay": "0.2s"
    },
    "HEADLINE348": {
        "type": "headline",
        "desktop.style.animation-name": "fadeInRight",
        "desktop.style.animation-delay": "0.2s",
        "mobile.style.animation-name": "fadeInRight",
        "mobile.style.animation-delay": "0.2s"
    },
    "GROUP345": {
        "type": "group",
        "option.data_event": [
            {
                "type": "phone",
                "action": "0856860860",
                "action_type": "action"
            }
        ],
        "desktop.option.sticky": true,
        "desktop.option.sticky_position": "bottom_left",
        "desktop.option.sticky_position_top": "0px",
        "desktop.option.sticky_position_left": "16px",
        "desktop.option.sticky_position_bottom": "9px",
        "desktop.option.sticky_position_right": "0px",
        "mobile.option.sticky": true,
        "mobile.option.sticky_position": "bottom_left",
        "mobile.option.sticky_position_top": "0px",
        "mobile.option.sticky_position_left": "16px",
        "mobile.option.sticky_position_bottom": "13px",
        "mobile.option.sticky_position_right": "0px"
    },
    "BOX367": {
        "type": "box",
        "option.data_event": [
            {
                "type": "link",
                "action": "https://zalo.me/0565771771",
                "action_type": "action"
            }
        ],
        "desktop.option.sticky": true,
        "desktop.option.sticky_position": "bottom_left",
        "desktop.option.sticky_position_top": "0px",
        "desktop.option.sticky_position_left": "15px",
        "desktop.option.sticky_position_bottom": "171px",
        "desktop.option.sticky_position_right": "0px",
        "desktop.style.animation-name": "pulse",
        "desktop.style.animation-delay": "1s",
        "mobile.option.sticky": true,
        "mobile.option.sticky_position": "bottom_left",
        "mobile.option.sticky_position_top": "0px",
        "mobile.option.sticky_position_left": "15px",
        "mobile.option.sticky_position_bottom": "129px",
        "mobile.option.sticky_position_right": "0px",
        "mobile.style.animation-name": "pulse",
        "mobile.style.animation-delay": "1s"
    }
}


</script>

<script id="script_ladipage_run" type="text/javascript">(function () {
        var run = function () {
            if (
                typeof window.LadiPageScript == "undefined" ||
                window.LadiPageScript == undefined ||
                typeof window.ladi == "undefined" || window.ladi == undefined
            ) {
                setTimeout(run, 100);
                return;
            }
            window.LadiPageApp = window.LadiPageApp || new window.LadiPageAppV2();
            window.LadiPageScript.runtime.is_mobile_only = false;
            window.LadiPageScript.runtime.bodyFontSize = 12;
            window.LadiPageScript.runtime.time_zone = 7;
            window.LadiPageScript.runtime.currency = "VND";
            window.LadiPageScript.runtime.convert_replace_str = true;
            window.LadiPageScript.runtime.desktop_width = 960;
            window.LadiPageScript.runtime.mobile_width = 420;
            window.LadiPageScript.run(true);
            window.LadiPageScript.runEventScroll();
        };
        run();
    })();
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="/web/js/modal-loading.js"></script>

<div id="fb-root"></div>
<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat"></div>
<script>
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "105895035575299");
        chatbox.setAttribute("attribution", "biz_inbox");
</script>
<!-- Your SDK code -->
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v18.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    function loadingOut(loading) {
        setTimeout(() => loading.out(), 3500);
    }
    $( document ).ready(function() {
        $('#submit_booking').click(function () {
        var loading = new Loading();
        loadingOut(loading);
        })
    });
</script>
</body>
</html>
