<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>THÔNG BÁO ĐẶT LỊCH THUÊ XE</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:100,500,300,700,400'>
    <style>
        body {
            display: table;
            width: 100%;
            background: #dedede;
            text-align: center;
        }

        * {
            -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing: border-box; /* Firefox, other Gecko */
            box-sizing: border-box; /* Opera/IE 8+ */
        }

        .aa_h2 {
            font-size: 1.2rem;
            text-transform: uppercase;
        }

        table {
            background: #fff;
        }

        table,
        thead,
        tbody,
        tfoot,
        tr,
        td,
        th {
            text-align: center;
            margin: auto;
            border: 1px solid #dedede;
            padding: 1rem;
            width: 50%;
        }

        .table {
            display: table;
            width: 100%;
        }

        .tr {
            display: table-row;
        }

        .thead {
            display: table-header-group;
        }

        .tbody {
            display: table-row-group;
        }

        .tfoot {
            display: table-footer-group;
        }

        .col {
            display: table-column;
        }

        .colgroup {
            display: table-column-group;
        }

        .td,
        .th {
            display: table-cell;
            width: 50%;
        }
        .td {
            font-size: 8px;
        }
        .th {
            font-size: 12px;
        }
        .table,
        .thead,
        .tbody,
        .tfoot,
        .tr,
        .td,
        .th {
            text-align: left;
            margin: auto;
            padding: 0.5rem;
        }

        .table {
            background: #fff;
            margin: auto;
            border: none;
            padding: 0;
            margin-bottom: 5rem;
            width: 100%;
        }

        .th {
            font-weight: 700;
            border: 1px solid #dedede;

        }
        .td {
            font-weight: 300;
            border: 1px solid #dedede;
            border-top: none;
        }

        .aa_css {
            background: skyblue;
            padding:0.5rem;
            display: table;
            width: 100%;
            height: 100vh;
            vertical-align: middle;
        }

        .ahmadawais {
            display: table;
            width: 100%;
            font: 100 1.2rem/2 Roboto;
            margin: 2rem auto;
        }

        svg {
            width: 32px;
            height: 32px;
            fill: #1da1f2;
        }

    </style>
</head>
<body>
<div class="aa_css">
    <p class="aa_h2" style="color: #f0f2f5;font-weight: 600; text-align: center">THÔNG BÁO LỊCH ĐẶT XE</p>
    <div class="table">
        <div class="thead">
            <div class="tr">
                <div class="th">Tên Khách Hàng :</div>
                <div class="td">{{ $name }}</div>
            </div>
        </div>
        <div class="tbody">
            <div class="tr">
                <div class="th">
                    Số Điện Thoại :
                </div>
                <div class="td">
                    {{ $phone }}
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Điểm Đón :
                </div>
                <div class="td">
                    {{ $address }}
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Điểm Đến :
                </div>
                <div class="td">
                    {{ $to_address }}
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Email :
                </div>
                <div class="td">
                    {{ $email ?? '' }}
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Thời Gian :
                </div>
                <div class="td">
                    {{ $date_time }}
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Loại Xe :
                </div>
                <div class="td">
                    @if ($type_car == 4)
                        Xe 4 chỗ
                    @elseif($type_car == 7)
                        Xe 7 chỗ
                    @else
                        Xe bán tải
                    @endif
                </div>
            </div>
            <div class="tr">
                <div class="th">
                    Ghi Chú :
                </div>
                <div class="td">
                    {{ $note ?? '' }}
                </div>
            </div>
        </div>
    </div>

    <div class="ahmadawais" style="text-align: center">
        <a href="{{ env('APP_URL') }}" target="_top">https://xeghephanoivinhphuc24h.net</a>
    </div>
</div>
</body>
</html>
