<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Mail\MailBooking;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if (!empty($data)) {
                $createMessage = Message::create([
                    'name' => $data['name'],
                    'email' => isset($data['email']) ? $data['email'] : null,
                    'address' => $data['address'],
                    'to_address' => $data['to_address'],
                    'note' => isset($data['note']) ? $data['note'] : null,
                    'date_time' => $data['date_time'],
                    'type_car' => $data['type_car'] ?? 4,
                    'phone' => $data['phone'],
                ]);
                if (!empty($createMessage)) {
                    Mail::to(isset($data['email']) ? $data['email'] : env('MAIL_FROM_ADDRESS'))
                        ->cc(env('MAIL_FROM_ADDRESS'))
                        ->send(
                            new MailBooking($createMessage)
                        );
                }

                return redirect()->back();
            }
        } catch (\Throwable $th) {
            logger()->debug($th);
        }
    }
}
