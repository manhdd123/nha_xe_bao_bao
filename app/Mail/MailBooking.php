<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Message;

class MailBooking extends Mailable
{
    use Queueable, SerializesModels;

    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("xeghephanoivinhphuc24h")
            ->view('mails.booking')
            ->with([
                'name' => $this->message->name,
                'email' => isset($this->message->email) ? $this->message->email : null,
                'address' => $this->message->address,
                'to_address' => $this->message->to_address,
                'note' => isset($this->message->note) ? $this->message->note : null,
                'date_time' => $this->message->date_time,
                'type_car' => $this->message->type_car,
                'phone' => $this->message->phone,
            ]);
    }
}
